/****************************************************************************************************************************************************************************************
 PROGRAMA: clsEmail.cs
 VERSION : 1.0
 OBJETIVO: Clase de envío de email
 FECHA   : 19/03/2022
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CEN;
using MailKit.Security;
using MimeKit;
using MimeKit.Utils;

namespace AppConfiguracion
{
  
    public class clsEmail
    {
        private string pclase = "clsEmail";
        // Envio correo sin Adjunto ni Logo

        // Envio correo con 2 o 3 Adjuntos con Logo
        public bool EnvioCorreo3(EN_Email oEmail, string[] strPara, string[] strCC, string[] strArchivoAdjunto, string rutalogo)
        {

            // FUNCION: ENVIAR CORREO A CLIENTE


            MailKit.Net.Smtp.SmtpClient smtp = new MailKit.Net.Smtp.SmtpClient();  //SMTP DE LIBRERIA MAILKIT CONECTOR PARA ENVIO DE CORREO 
            var email = new MimeMessage();                                          //MENSAJE
            var builder = new BodyBuilder ();                                       //BODY CONTRUCTOR
            var BodyHtmlView ="";                                                   //CUERPO DEL MENSAJE    
            int i;                                                                  //IDENTIFICADOR PARA CUENTA DE CORREO DESTINO
            
            try
            {
                if(oEmail.PuertoSMTP==587)
                {
                    smtp.Connect(oEmail.CuentaSMTP,oEmail.PuertoSMTP, SecureSocketOptions.None);
                }else{
                    smtp.Connect(oEmail.CuentaSMTP,oEmail.PuertoSMTP, SecureSocketOptions.SslOnConnect);
                }
               
                smtp.Authenticate(oEmail.CuentaDe, oEmail.ClaveDe);
                email.From.Add(new MailboxAddress( oEmail.NombreDe,oEmail.CuentaDe));
    
                if (strPara != null)
                {
                    for (i = 0; i <= strPara.Length - 1; i++)
                        email.To.Add(MailboxAddress.Parse(strPara[i]));// Cuenta de Correo al que se le quiere enviar el e-mail
                }
            
                email.Subject = oEmail.Asunto; // Sujeto del e-mail
                
               // Adicionando logo
                var image = builder.LinkedResources.Add(@""+rutalogo);
                image.ContentId = MimeUtils.GenerateMessageId ();

                BodyHtmlView= string.Format(@"<img src=""cid:{0}"" align=""center"" /><br />" + oEmail.Mensaje,  image.ContentId);
                builder.HtmlBody = BodyHtmlView;

                foreach (var files in strArchivoAdjunto)
                {
                    builder.Attachments.Add(files.ToString());
                }

                email.Body = builder.ToMessageBody();

                smtp.Send(email);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                return false;
            }
            finally
            {
                smtp.Disconnect(true);
            }
        }


        // Envio correo con excel error
        public bool EnvioCorreoError(EN_Email oEmail, string[] strPara, string[] strCC, string[] strArchivoAdjunto)
        {

            // FUNCION: ENVIAR CORREO A SISTEMAS DE CAIDAS


            MailKit.Net.Smtp.SmtpClient smtp = new MailKit.Net.Smtp.SmtpClient();  //SMTP DE LIBRERIA MAILKIT CONECTOR PARA ENVIO DE CORREO 
            var email = new MimeMessage();                                          //MENSAJE
            var builder = new BodyBuilder ();                                       //BODY CONTRUCTOR
            var BodyHtmlView ="";                                                   //CUERPO DEL MENSAJE    
            int i;                                                                  //IDENTIFICADOR PARA CUENTA DE CORREO DESTINO
            
            try
            {
                if(oEmail.PuertoSMTP==587)
                {
                    smtp.Connect(oEmail.CuentaSMTP,oEmail.PuertoSMTP, SecureSocketOptions.None);
                }else{
                    smtp.Connect(oEmail.CuentaSMTP,oEmail.PuertoSMTP, SecureSocketOptions.SslOnConnect);
                }
               
                smtp.Authenticate(oEmail.CuentaDe, oEmail.ClaveDe);
                email.From.Add(new MailboxAddress( oEmail.NombreDe,oEmail.CuentaDe));
    
                if (strPara != null)
                {
                    for (i = 0; i <= strPara.Length - 1; i++)
                        email.To.Add(MailboxAddress.Parse(strPara[i]));// Cuenta de Correo al que se le quiere enviar el e-mail
                }
            
                email.Subject = oEmail.Asunto; // Sujeto del e-mail
                

                BodyHtmlView= string.Format(@"" + oEmail.Mensaje);
                builder.HtmlBody = BodyHtmlView;

                foreach (var files in strArchivoAdjunto)
                {
                    builder.Attachments.Add(files.ToString());
                }

                email.Body = builder.ToMessageBody();

                smtp.Send(email);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                return false;
            }
            finally
            {
                smtp.Disconnect(true);
            }


        
        }
    }
}