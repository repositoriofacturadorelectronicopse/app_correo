﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: FuncionesS3.cs
 VERSION : 1.0
 OBJETIVO: Clase de funciones comunes de S3
 FECHA   : 21/01/2022
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using Amazon.S3;
using Amazon.S3.Model;
using System.Linq;
using CEN;
using System;
using Amazon;
using System.Threading;
using System.IO;
using System.Threading.Tasks;

namespace AppConfiguracion
{
    public static class FuncionesS3
    {

        public static async Task CreateFoldersAsync(string path)
        {
            //DESCRIPCION: CREAR FOLDER EN S3

            try
            {
                path = EnsureEndsWithCaracter(path, "/");
                
                var newRegion = RegionEndpoint.GetBySystemName(EN_ConfigConstantes.Instance.const_s3_region);
          
                IAmazonS3 client =
                    new AmazonS3Client(
                    EN_ConfigConstantes.Instance.const_s3_accessKey,
                    EN_ConfigConstantes.Instance.const_s3_secretKey,  
                    newRegion
                    
                    );

                var findFolderRequest = new ListObjectsV2Request();
                findFolderRequest.BucketName = EN_ConfigConstantes.Instance.const_s3_bucketName;
                findFolderRequest.Prefix = path;
                findFolderRequest.MaxKeys = 1;

                ListObjectsV2Response findFolderResponse = 
                await client.ListObjectsV2Async(findFolderRequest);


                if (findFolderResponse.S3Objects.Any())
                {
                    return;
                }
                
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = EN_ConfigConstantes.Instance.const_s3_bucketName,
                    StorageClass = S3StorageClass.Standard,
                    ServerSideEncryptionMethod = ServerSideEncryptionMethod.None,
                    Key = path, 
                    ContentBody = string.Empty
                };

                // add try catch in case you have exceptions shield/handling here 
                PutObjectResponse response = await client.PutObjectAsync(request);

             }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
        }

        public static async Task UploadFile(string destinoFilePathS3, string origenFilePath)
        {
          
              var newRegion = RegionEndpoint.GetBySystemName(EN_ConfigConstantes.Instance.const_s3_region);
          
                IAmazonS3 client =
                    new AmazonS3Client(
                    EN_ConfigConstantes.Instance.const_s3_accessKey,
                    EN_ConfigConstantes.Instance.const_s3_secretKey,  
                    newRegion
                    
                    );
            try
            {
                PutObjectRequest putRequest = new PutObjectRequest
                {
                    BucketName = EN_ConfigConstantes.Instance.const_s3_bucketName,
                    Key = destinoFilePathS3,
                    FilePath = origenFilePath,
                    ContentType = "text/plain"
                };

                PutObjectResponse response = await client.PutObjectAsync(putRequest);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Check the provided AWS S3 Credentials.");
                }
                else
                {
                    throw new Exception("Error occurred S3: " + amazonS3Exception.Message);
                }
            }
        }

         public static async Task DownloadFile(string s3FilePath, string destinoFilePath)
        {
          
              var newRegion = RegionEndpoint.GetBySystemName(EN_ConfigConstantes.Instance.const_s3_region);
          
                IAmazonS3 client =
                    new AmazonS3Client(
                    EN_ConfigConstantes.Instance.const_s3_accessKey,
                    EN_ConfigConstantes.Instance.const_s3_secretKey,  
                    newRegion
                    
                    );
            try
            {
                GetObjectRequest getRequest = new GetObjectRequest
                {
                    BucketName = EN_ConfigConstantes.Instance.const_s3_bucketName,
                    Key = s3FilePath,
                };

                using(GetObjectResponse response = await client.GetObjectAsync(getRequest))
                {
                    CancellationTokenSource source = new CancellationTokenSource();
                    CancellationToken token = source.Token;
                    await response.WriteResponseStreamToFileAsync(destinoFilePath, false, token);
                }



            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Check the provided AWS S3 Credentials.");
                }
                else
                {
                    throw new Exception("Error occurred S3: " + amazonS3Exception.Message);
                }
            }
        }



        public static string EnsureEndsWithCaracter(this string text, string caracter)
        {
            try
            {
                if (!text.EndsWith(caracter))
                {
                    return string.Format("{0}{1}", text, caracter);
                }

                return text;
                 
            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
          
        }


        public static async Task verificaLocalExtraeS3Async(string rutaS3, string rutaLocal)
        {
            //DESCRIPCION: VERIFICA SI EXISTE EN LOCAL SI NO EXTRAE DE S3


            try
            {
                
                var newRegion = RegionEndpoint.GetBySystemName(EN_ConfigConstantes.Instance.const_s3_region);
          
                IAmazonS3 client =
                    new AmazonS3Client(
                    EN_ConfigConstantes.Instance.const_s3_accessKey,
                    EN_ConfigConstantes.Instance.const_s3_secretKey,  
                    newRegion
                    
                    );

                if (!File.Exists(rutaLocal))
                {

                    var findFolderRequest = new ListObjectsV2Request();
                    findFolderRequest.BucketName = EN_ConfigConstantes.Instance.const_s3_bucketName;
                    findFolderRequest.Prefix = rutaS3;
                    findFolderRequest.MaxKeys = 1;

                    ListObjectsV2Response findFolderResponse = 
                    await client.ListObjectsV2Async(findFolderRequest);


                    if (findFolderResponse.S3Objects.Any())
                    {
                         await DownloadFile(rutaS3,rutaLocal);  
                    }
                   
                }
                
          

             }
            catch (System.Exception ex)
            {
                
                throw ex;
            }


        }

        public static void InitialS3(string numeroRuc, string nombreImagen)
        {
            try
            {
                string rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                string s3Img = string.Format("/{0}/{1}",numeroRuc,nombreImagen);
                string localImg = string.Format("{0}/{1}/{2}",rutabase,numeroRuc,nombreImagen);
                string s3Ruc = string.Format("/{0}/", numeroRuc);

                //verificamos si existe carpeta en S3, de no existir, crear
                AppConfiguracion.FuncionesS3.CreateFoldersAsync(s3Ruc).Wait();
                AppConfiguracion.FuncionesS3.CreateFoldersAsync(s3Ruc+ EN_Constante.g_const_rutaSufijo_xml).Wait();
                AppConfiguracion.FuncionesS3.CreateFoldersAsync(s3Ruc+ EN_Constante.g_const_rutaSufijo_pdf).Wait();
                AppConfiguracion.FuncionesS3.CreateFoldersAsync(s3Ruc+ EN_Constante.g_const_rutaSufijo_cdr).Wait();

                AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(s3Img,localImg).Wait(); 
                 
            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
          
        }

        public static void CopyS3DeleteLocal(string numeroRuc, string rutaBase, string sufijoCarpeta, string nombreArchivo, string extensionArchivo)
        {
            //DESCRIPCION: COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL

            try
            {
                string nombreXml    = sufijoCarpeta + nombreArchivo + extensionArchivo;
                string s3RutaFile   = string.Format("/{0}/{1}",numeroRuc, nombreXml);
                string localRutaFile= string.Format("{0}/{1}/{2}",rutaBase ,numeroRuc,nombreXml);
                
                if (File.Exists(localRutaFile))
                {
                    AppConfiguracion.FuncionesS3.UploadFile( s3RutaFile,localRutaFile).Wait();
                    File.Delete(localRutaFile);
                }
            
                 
            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
        


        }
    
    
    
    }
}