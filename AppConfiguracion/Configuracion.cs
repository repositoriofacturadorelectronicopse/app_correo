﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: Configuracion.cs
 VERSION : 1.0
 OBJETIVO: Clase de funciones comunes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System.Net;
using CEN;

namespace AppConfiguracion
{
    public class Configuracion
    {


      

    public string GetCadenaConexionFromWA(string p_url,string p_puerto,string p_api, string p_region,  string p_version, string p_secrectname)
       {
        // DESCRIPCION: FUNCION PARA OBTENER CADENA DE CONEXION DESDE API SERVICE MANAGER


           string cadenaConexion= EN_Constante.g_const_vacio;
           string urlWA     = EN_Constante.g_const_vacio;
       

           try
           {
            
                urlWA= p_url+":"+p_puerto+"/"+p_api;


                using(WebClient webClient = new WebClient())
                {
                    webClient.QueryString.Add("secretName", p_secrectname);
                    webClient.QueryString.Add("region", p_region);
                    webClient.QueryString.Add("versionStage", p_version);
                    cadenaConexion = webClient.DownloadString(urlWA);
                    return cadenaConexion;

                }
                
            
           }
           catch (System.Exception ex)
           {
               cadenaConexion = EN_Constante.g_const_vacio;
               return cadenaConexion;
               
           }
       }



    
    
    
    
    }
}