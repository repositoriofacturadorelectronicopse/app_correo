
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
# WORKDIR /home/jalarcon/Documentos/FacturacionElectronica/

WORKDIR /app

# Copy csproj and restore as distinct layers
COPY app_correo/*.csproj ./



#RUN dotnet restore

# Copy everything else and build
COPY . ./
#RUN dotnet publish -c Release -o out
RUN dotnet publish app_correo.sln -c Release -o out

#copiamos el appsettings.json
# COPY BatchFactura/appsettings.json  ./
#COPY app_correo/appsettings.json  /

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build-env /app/out .
# ENTRYPOINT ["dotnet", "SAASWeb.dll"]


# Install cron
RUN apt-get update -qq && apt-get -y install cron -qq --force-yes

#SET TIME ZONE IN DOCKER
RUN apt-get update && \
    apt-get install -yq tzdata && \
    ln -fs /usr/share/zoneinfo/America/Bogota /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata


# Add export environment variable script and schedule
COPY *.sh ./
COPY schedule /etc/cron.d/schedule

RUN sed -i 's/\r//' export_env.sh \
    && sed -i 's/\r//' run_app_correo_docs.sh \
    && sed -i 's/\r//' /etc/cron.d/schedule \
    && chmod +x export_env.sh run_app_correo_docs.sh \
    && chmod 0644 /etc/cron.d/schedule


# Create log file
RUN touch /var/log/cron.log
RUN chmod 0666 /var/log/cron.log


# Volume required for tail command
VOLUME /var/log

# Run Cron
CMD /app/export_env.sh && /usr/sbin/cron && tail -f /var/log/cron.log
