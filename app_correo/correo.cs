﻿using System;
using AppServicioCorreo;
using AppServicioDoc;
using CEN;
using CLN;
using CAD;
using Newtonsoft.Json;
using System.Globalization;
using System.Threading;

namespace app_correo
{
    class correo
    {

        public correo( ){
        
        } 
        
        static void Main(string[] args)
        {

            var culture = new System.Globalization.CultureInfo("es-PE");
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes();
            daoConfConst.guardar_ConfigConstantes();

            var correo = new correo();

            // Envío de correos pendientes de boletas, facturas y notas  
            correo.CrearCorreosPendientes();

            //Envío de correos pendientes de boletas, facturas y notas  
            correo.CrearCorreosOtroscpePendientes();
            
            //Envío de correos pendientes de guías de remisión
            correo.CrearCorreosPendientesGuia();
            
            //Envío de correos pendientes de errores de sistema
            correo.CrearCorreosErrorSistemas();
            
            
        }


         public void CrearCorreosPendientes()
        {
            //DESCRIPCION: CREAR CORRESOS CrearCorreosPendientes
             
           
            Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            GenerarCorreo oCorreo = new GenerarCorreo(); // generar correo 
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error


            try
            {
               
                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    EN_Constante.g_const_0, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_generarCpe, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    oCorreo.getDateNowLima(),oCorreo.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);

                
                    oCorreo.CrearCorreoEnvio();


                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1,
                                        respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                        EN_Constante.g_const_valExito, oCorreo.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {

                  
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_6, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, e.Message.ToString(), 
                                    oCorreo.getDateHourNowLima(),  JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));


                
            }
        }

          public void CrearCorreosOtroscpePendientes()
        {
            //DESCRIPCION: CREAR CORRESOS CrearCorreosPendientes
             
           
            Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            GenerarCorreo oCorreo = new GenerarCorreo(); // generar correo 
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error


            try
            {
               
                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    EN_Constante.g_const_0, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_generarOtrosCpe, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    oCorreo.getDateNowLima(),oCorreo.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);

                
                    oCorreo.CrearCorreoOtroscpeEnvio();


                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1,
                                        respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                        EN_Constante.g_const_valExito, oCorreo.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {

                  
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_6, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, e.Message.ToString(), 
                                    oCorreo.getDateHourNowLima(),  JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));


                
            }
        }


        

        
        
        public void CrearCorreosPendientesGuia() 
        {
            //DESCRIPCION: Proceso de envio de Correo de Guias.
            Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            GenerarCorreo oCorreo = new GenerarCorreo(); // generar correo 
            EN_RespuestaData response = new EN_RespuestaData();

            try
            {
              ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    EN_Constante.g_const_0, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_enviarCorreoGuia, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    oCorreo.getDateNowLima(),oCorreo.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
            
            response = oCorreo.CrearCorreoEnvioGuia();
            if (response.ResplistaGuia.FlagVerificacion) 
            {
                response.ResplistaGuia.DescRespuesta = EN_Constante.g_const_email_exito;
                response.errorService.TipoError = EN_Constante.g_const_0;
                response.errorService.CodigoError = EN_Constante.g_const_2000;
                response.errorService.DescripcionErr =EN_Constante.g_const_consultaexitosa;

            } 
            
            adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_3,
                                        response.ResplistaGuia.DescRespuesta, 
                                        EN_Constante.g_const_valExito, oCorreo.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(response), JsonConvert.SerializeObject(EN_Constante.g_const_0));
            
            }
            catch (Exception ex)
            {
                response.ResplistaGuia.FlagVerificacion = false;
                response.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                response.errorService.TipoError = EN_Constante.g_const_1;
                response.errorService.CodigoError = EN_Constante.g_const_3000;
                response.errorService.DescripcionErr = ex.Message;
                
                 adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, ex.Message.ToString(), 
                                    oCorreo.getDateHourNowLima(),  JsonConvert.SerializeObject(response), JsonConvert.SerializeObject(EN_Constante.g_const_0));
            }
        }

          public void CrearCorreosErrorSistemas()
        {
            //DESCRIPCION: CREAR CORRESOS CrearCorreosPendientes
             
           
            Int32 ntraLog = EN_Constante.g_const_0;                 // Número de transacción
            EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
            
            AD_clsLog adlog =  new AD_clsLog();                     // clase Log
            GenerarCorreo oCorreo = new GenerarCorreo(); // generar correo 
            EN_Concepto concepto;                                   //entidad concepto
            NE_Consulta consulta = new NE_Consulta();               // consulta
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error


            try
            {
               
                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa,EN_Constante.g_const_0.ToString(),EN_Constante.g_const_null, 
                                                    EN_Constante.g_const_0, EN_Constante.g_const_0,  EN_Constante.g_const_vacio, EN_Constante.g_const_1,
                                                    EN_Constante.g_const_funcion_generarCorreoErrores, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                    oCorreo.getDateNowLima(),oCorreo.getDateHourNowLima(),
                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);

                
                    oCorreo.CrearCorreoEnvioErrores();


                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1,
                                        respuesta.RptaRegistroCpeDoc.DescripcionResp, 
                                        EN_Constante.g_const_valExito, oCorreo.getDateHourNowLima(),
                                                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));
                    

            }
            catch (System.Exception e)
            {

                  
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.LlenarErrorWebSer(EN_Constante.g_const_menos1, concepto.conceptocorr, e.Message.ToString());
                    respuesta.ErrorWebService = ErrorWebSer;

                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_6, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, e.Message.ToString(), 
                                    oCorreo.getDateHourNowLima(),  JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(EN_Constante.g_const_0));


                
            }
        }
    }

    
}
