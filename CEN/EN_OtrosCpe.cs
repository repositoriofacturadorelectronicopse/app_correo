﻿using System;
using System.Collections.Generic;


namespace CEN
{

    public class EN_OtrosCpe
    {
        public int empresa_id {get;set;}
        public int puntoventa_id  {get;set;}
        public string id {get;set;}
        public string tipodocumento_id {get;set;}
        public string desctipodocumento  {get;set;}
        public string serie {get;set;}
        public string numero {get;set;}
        public string fechaemision {get;set;}
        public string persona_tipodoc {get;set;}
        public string persona_nrodoc {get;set;}
        public string persona_nombrecomercial  {get;set;}
     
        
        public string persona_ubigeo {get;set;}
      

       
        public string persona_direccion {get;set;}

  
        public string persona_urbanizacion {get;set;}

        
        public string persona_departamento  {get;set;}
       

        
        public string persona_provincia {get;set;}
     

        
        public string persona_distrito  {get;set;}
        

        
        public string persona_codpais {get;set;}
    

        
        public string persona_descripcion {get;set;}
       

        
        public string persona_email {get;set;}
       

        public string regimen_retper {get;set;}

        
        public decimal tasa_retper {get;set;}

        
        public string observaciones {get;set;}

        
        public decimal importe_retper {get;set;}

        
        public string moneda_impretper {get;set;}

        
        public decimal importe_pagcob {get;set;}

        
        public string moneda_imppagcob {get;set;}

        
        public string estado {get;set;}

        
        public string situacion {get;set;}

        
        public string enviado_email {get;set;}
        
        public string enviado_externo {get;set;}

        // Para Busquedas
        private string _fechaIni;
        public string fechaIni
        {
            get
            {
                return _fechaIni;
            }
            set
            {
                _fechaIni = value;
            }
        }

        private string _fechaFin;
        public string fechaFin
        {
            get
            {
                return _fechaFin;
            }
            set
            {
                _fechaFin = value;
            }
        }

        private string _nrodocempresa;
        public string nrodocempresa
        {
            get
            {
                return _nrodocempresa;
            }
            set
            {
                _nrodocempresa = value;
            }
        }

        private string _descripcionempresa;
        public string descripcionempresa
        {
            get
            {
                return _descripcionempresa;
            }
            set
            {
                _descripcionempresa = value;
            }
        }

        // Informacion Auditoria
        private string _UsuarioSession;
        public string UsuarioSession
        {
            get
            {
                return _UsuarioSession;
            }
            set
            {
                _UsuarioSession = value;
            }
        }

        private string _CadenaAleatoria;
        public string CadenaAleatoria
        {
            get
            {
                return _CadenaAleatoria;
            }
            set
            {
                _CadenaAleatoria = value;
            }
        }

        // Para Errores Rechazos
        private string _MensajeError;
        public string MensajeError
        {
            get
            {
                return _MensajeError;
            }
            set
            {
                _MensajeError = value;
            }
        }

        private string _FechaEnvio;
        public string FechaEnvio
        {
            get
            {
                return _FechaEnvio;
            }
            set
            {
                _FechaEnvio = value;
            }
        }

        private string _FechaError;
        public string FechaError
        {
            get
            {
                return _FechaError;
            }
            set
            {
                _FechaError = value;
            }
        }

        // Para reportes
        private string _Cantidad;
        public string Cantidad
        {
            get
            {
                return _Cantidad;
            }
            set
            {
                _Cantidad = value;
            }
        }

        // Ultimos Parametros Agregados
        private string _nombreXML;
        public string nombreXML
        {
            get
            {
                return _nombreXML;
            }
            set
            {
                _nombreXML = value;
            }
        }
        private string _vResumen;
        public string vResumen
        {
            get
            {
                return _vResumen;
            }
            set
            {
                _vResumen = value;
            }
        }
        private string _vFirma;
        public string vFirma
        {
            get
            {
                return _vFirma;
            }
            set
            {
                _vFirma = value;
            }
        }
        private string _xmlEnviado;
        public string xmlEnviado
        {
            get
            {
                return _xmlEnviado;
            }
            set
            {
                _xmlEnviado = value;
            }
        }
        private string _xmlCDR;
        public string xmlCDR
        {
            get
            {
                return _xmlCDR;
            }
            set
            {
                _xmlCDR = value;
            }
        }
        private string _Mensaje;
        public string Mensaje
        {
            get
            {
                return _Mensaje;
            }
            set
            {
                _Mensaje = value;
            }
        }

        // Para Validar Usuario Certificado Empresa
        private string _Usuario;
        public string Usuario
        {
            get
            {
                return _Usuario;
            }
            set
            {
                _Usuario = value;
            }
        }

        private string _Clave;
        public string Clave
        {
            get
            {
                return _Clave;
            }
            set
            {
                _Clave = value;
            }
        }
    }

    public class OtroscpeSunat : EN_OtrosCpe
    {
        public OtroscpeSunat()
        {
            ListaDetalleOtrosCpe = new List<EN_DetalleOtrosCpe>();
        }
        public string ValorResumen { get; set; }
        public string ValorFirma { get; set; }
        public string NombreFichero { get; set; }
        public List<EN_DetalleOtrosCpe> ListaDetalleOtrosCpe { get; set; }
    }

}