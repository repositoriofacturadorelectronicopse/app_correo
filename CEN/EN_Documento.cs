/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Documento.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad documento
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations;



namespace CEN
{
    public class EN_Documento
    {

        public int Idempresa {get;set;}         // ide de empresa
        public string Id {get;set;}             // id de documento
        public string Idtipodocumento {get;set;}    // id de tipo de documento
         public string Tipodocumento {get;set;}     // tipo de documento
        public string Serie {get;set;}          // serie de comprobante
        public string Numero {get;set;}         // número de comprobante
        public int Idpuntoventa {get;set;}      // id de punto de venta
        public string Emailcliente {get;set;}   // email del cliente
         public string Nombrecliente {get;set;} // nombre de cliente
        public string Fecha {get;set;}          // fecha
        public string Hora {get;set;}           // hora
        public string Fechavencimiento {get;set;}   // fecha de vencimiento
        public string Estado {get;set;}         // estado
        public string Situacion {get;set;}      // situcación
        public string EnviadoMail {get;set;}    // enviado de email

         public double Importefinal {get;set;}         // IMPORTE FINAL


    }
}