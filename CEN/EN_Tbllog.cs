namespace CEN
{
    public class EN_Tbllog
    {
        public int logcodi { get; set;}
        public string logprog { get; set;}
        public string  logidoc { get; set;}
        public string logtdoc { get; set;}
        public int  logiemp { get; set;}
        public int  logipvt { get; set;}
        public string lognemp { get; set;}
        public int  lognive { get; set;}
        public string lognwba { get; set;}
        public string logdurl { get; set;}
        public int  logtser { get; set;}
        public int  logtipo { get; set;}
        public int logferr { get; set;}
        public int logoerr { get; set;}
        public string logerro { get; set;}
        public string logderr { get; set;}
        public string logfech { get; set;}
        public string loghini { get; set;}
        public string logdate { get; set;}
        public string loghfin { get; set;}
        public string logdats { get; set;}
        public string loguser { get; set;}


          
	
    }
}