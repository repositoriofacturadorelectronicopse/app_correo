namespace CEN
{
    public class EN_Guia
    {
        public string id {get;set;}  //id GUIA
        public int empresa_id {get;set;}  //id de empresa
        public string tipodocumento_id {get;set;}  //tipo de documento
        public string serie {get;set;}  // serie de guia
        public string numero {get;set;}  // numero de guia
        public int puntoemision {get;set;}  //punto de emision
        public string destinatario_email {get;set;}  //email de destinatario
        public string fechaemision {get;set;}  //fecha de emision
        public string horaemision {get;set;}  //hora de emision
        public string estado {get;set;}  // estado
        public string situacion {get;set;}  //situacion
        public string enviado_email {get;set;}  // email enviado
        public string destinatario_nombre {get;set;}  // nombre de destinatario
        public string tipodocumento {get;set;}  // tipo de documento
    }
}