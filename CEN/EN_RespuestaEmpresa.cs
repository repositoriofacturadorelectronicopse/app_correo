using System.Collections.Generic;
namespace CEN
{
    public class EN_RespuestaEmpresa
    {
          public EN_RespuestaRegistro RespRegistro{ get; set; } 
         public EN_EmpresaGuia objEmpresa { get; set; } 
         public List<EN_EmpresaGuia> listaEmpresa = new List<EN_EmpresaGuia>();
         public EN_ErrorWebService errorService { get; set; } 
         public EN_RespuestaEmpresa()
         {
             RespRegistro = new EN_RespuestaRegistro();
             objEmpresa = new EN_EmpresaGuia();
             errorService = new EN_ErrorWebService();

         }
        
    }
}