
/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RespuestaRegistro.cs
 VERSION : 1.0
 OBJETIVO: Clase respuesta de registro
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_RespuestaRegistro
    {
          //DESCRIPCION: Respuesta de registro correo
        public bool FlagVerificacion { get; set; }  //Flag de verificacion de registro
        public int dato { get; set; }  //dato de registro
        public string DescRespuesta { get; set; }  //descripcion de respuesta
    }
}