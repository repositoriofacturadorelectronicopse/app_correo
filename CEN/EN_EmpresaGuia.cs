namespace CEN
{
    public class EN_EmpresaGuia
    {
        public int Id { get; set; } //  ID
        public string Tipoempresa { get; set; } 
        public string Nrodocumento { get; set; } 
        public string RazonSocial { get; set; } 
        public string Nomcomercial { get; set; } 
        public string Propietario { get; set; } 
        public string Tipodocumento { get; set; } 
        public string Nrodoc { get; set; } 
        public string Apellido { get; set; } 
        public string Nombre { get; set; } 
        public string Direccion { get; set; } 
        public string Sector { get; set; } 
        public string Telefono1 { get; set; } 
        public string Telefono2 { get; set; } 
        public string Email { get; set; } 
        public string Web { get; set; } 
        public string Imagen { get; set; } 
        public string Codpais { get; set; } 
        public string Coddep { get; set; } 
        public string Codpro { get; set; } 
        public string Coddis { get; set; } 
        public string Departamento { get; set; } 
        public string Provincia { get; set; } 
        public string Distrito { get; set; } 
        public string SignatureID { get; set; } 
        public string SignatureURI { get; set; } 
        public string NroResolucion { get; set; } 
        public string CuentaBcoDet { get; set; } 
        public string AplicaDctoItem { get; set; } 
        public string AplicaCargoItem { get; set; } 
        public string AplicaISC { get; set; } 
        public string MuestraIgv { get; set; } 
        public string MuestraSubTotal { get; set; } 
        public string AplicaDctoGlobal { get; set; } 
        public string AplicaCargoGlobal { get; set; } 
        public string Estado { get; set; } 
        public string Usureg { get; set; } 
        public string Borrado { get; set; } 
        public string UsuarioSession { get; set; } 
        public string CadenaAleatoria { get; set; } 
        public string Rutadocumento{get;set;}
        public string enviarxml{get;set;}
        public string Produccion {get; set;}
    }
}