/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Correo.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad correo
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
 public class EN_Correo
{

    public int id  {get;set;}                   //identificador
    public int empresa_id  {get;set;}           //identificador de empresa
    public string cuenta_email  {get;set;}      //cuenta de correo
    public string nombre_email  {get;set;}      //nombre de correo
    public string clave_email  {get;set;}       //clave de correo
    public string cuenta_smtp  {get;set;}       //smtp
    public int puerto_smtp  {get;set;}          //puerto smtp
    public string link_descarga  {get;set;}     //link de descarga
    public string es_default  {get;set;}        //cuenta por defecto 0:NO, 1:SI
    
    // Informacion Auditoria
    public string UsuarioSession  {get;set;}    //usuario dde session
    public string CadenaAleatoria  {get;set;}   //cadena aleatoria
}
}