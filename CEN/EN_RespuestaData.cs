namespace CEN
{
    public class EN_RespuestaData
    {
        public EN_RespuestaRegistro ResplistaGuia{ get; set; } 
        public EN_ErrorWebService errorService { get; set; } 
        public EN_RespuestaData () 
        {
            ResplistaGuia = new EN_RespuestaRegistro();
            errorService = new EN_ErrorWebService();

        }
    }
}