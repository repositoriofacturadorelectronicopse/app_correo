/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RespuestaListaCorreo.cs
 VERSION : 1.0
 OBJETIVO: Clase respuesta de correos 
 FECHA   : 24/11/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using System.Collections.Generic;
namespace CEN
{
    public class EN_RespuestaListaCorreo
    {
         //DESCRIPCION: Listar guia
        public EN_RespuestaRegistro ResplistaGuia{ get; set; } 
        public List<EN_Correo> listDetGuia = new List<EN_Correo>();

        public EN_RespuestaListaCorreo() {
            ResplistaGuia = new EN_RespuestaRegistro();
        }
        
    }
}