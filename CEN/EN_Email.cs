namespace CEN
{
public class EN_Email
{
    private string _CuentaDe;
    public string CuentaDe
    {
        get
        {
            return _CuentaDe;
        }
        set
        {
            _CuentaDe = value;
        }
    }
    private string _NombreDe;
    public string NombreDe
    {
        get
        {
            return _NombreDe;
        }
        set
        {
            _NombreDe = value;
        }
    }
    private string _ClaveDe;
    public string ClaveDe
    {
        get
        {
            return _ClaveDe;
        }
        set
        {
            _ClaveDe = value;
        }
    }
    private string _Asunto;
    public string Asunto
    {
        get
        {
            return _Asunto;
        }
        set
        {
            _Asunto = value;
        }
    }
    private string _Mensaje;
    public string Mensaje
    {
        get
        {
            return _Mensaje;
        }
        set
        {
            _Mensaje = value;
        }
    }
    private string _CuentaSMTP;
    public string CuentaSMTP
    {
        get
        {
            return _CuentaSMTP;
        }
        set
        {
            _CuentaSMTP = value;
        }
    }
    private int _PuertoSMTP;
    public int PuertoSMTP
    {
        get
        {
            return _PuertoSMTP;
        }
        set
        {
            _PuertoSMTP = value;
        }
    }
    private string _LinkDescarga;
    public string LinkDescarga
    {
        get
        {
            return _LinkDescarga;
        }
        set
        {
            _LinkDescarga = value;
        }
    }
}
}