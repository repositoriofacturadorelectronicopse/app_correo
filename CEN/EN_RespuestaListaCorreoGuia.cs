/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RespuestaListaCorreoGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase respuesta de guia 
 FECHA   : 24/11/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/
using System.Collections.Generic;
namespace CEN
{
    public class EN_RespuestaListaCorreoGuia
    {
        //DESCRIPCION: Listar guia
        public EN_RespuestaRegistro ResplistaGuia{ get; set; } 
        public List<EN_Guia> listDetGuia = new List<EN_Guia>();

        public EN_RespuestaListaCorreoGuia() {
            ResplistaGuia = new EN_RespuestaRegistro();
        }
    }
}