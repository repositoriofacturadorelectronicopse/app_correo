/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Constante.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad  constantes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_Constante
    {

        public const int g_const_menos1 = -1;   // Constante -1
        public const int g_const_0 = 0; // Constante 0
        public const int g_const_1 = 1; // Constante 1
         public const int g_const_2 = 2; // Constante 2
         public const int g_const_3 = 3; // Constante 3
         public const int g_const_4 = 4; // Constante 4
         public const int g_const_5 = 5; // Constante 5 
         public const int g_const_6 = 6; // Constante 6
         public const int g_const_7 = 7; // Constante 7 
         public const int g_const_8 = 8; // Constante 8
         public const int g_const_9 = 9; // Constante 9 
         public const int g_const_10 = 10; // Constante 10
         public const int g_const_11 = 11; // Constante 11 
         public const int g_const_20 = 20; // Constante 20
         public const int g_const_21 = 21; // Constante 21
         public const int g_const_22 = 22; // Constante 22
         public const int g_const_23 = 23; // Constante 23 
         public const int g_const_24 = 24; // Constante 24
         public const int g_const_25 = 25; // Constante 25
          
         public const int g_const_200 = 200; // Constante 200
         public const int g_const_201 = 201; // Constante 201 
         public const int g_const_202 = 202; // Constante 202
         public const int g_const_203 = 203; // Constante 203 
         public const int g_const_204 = 204; // Constante 204
         public const int g_const_205 = 205; // Constante 205
         
        public const string g_const_null = null; // Constante null
        public const string g_const_vacio = ""; // Constante vacio
        public const string g_const_formfech = "dd/MM/yyyy"; // formato de fecha
        public const string g_const_formhora = "HH:mm:ss"; // formato de hora
        public const string g_const_rango_num = "[0-9]"; // Rango número

        public const int g_const_prefijo_cpe_doc = 2; // Prefijo de comprobante electronico doc
        public const int  g_const_100 = 100; // Constante 100
        public const int  g_const_101 = 101; // Constante 101
        public const int  g_const_102 = 102; // Constante 102
        public const int  g_const_103 = 103; // Constante 103
        public const int  g_const_104= 104; // Constante 104
        public const int  g_const_105 = 105; // Constante 105
        public const int  g_const_106 = 106; // Constante 106
        public const int  g_const_107 = 107; // Constante 107
        public const int  g_const_108 = 108; // Constante 108
        public const int  g_const_109 = 109; // Constante 109
        public const int  g_const_110 = 110; // Constante 110
        public const int  g_const_111 = 111; // Constante 111
        public const int  g_const_112 = 112; // Constante 112
        public const int  g_const_113 = 113; // Constante 113
        public const int  g_const_114 = 114; // Constante 114
        public const int  g_const_115 = 115; // Constante 115
        public const int  g_const_116 = 116; // Constante 116
        public const int  g_const_117 = 117; // Constante 117
        public const int  g_const_118 = 118; // Constante 118
        public const int  g_const_119 = 119; // Constante 119
        public const int  g_const_150 = 150; // Constante 150
        public const int  g_const_151 = 151; // Constante 151
        public const int  g_const_152 = 152; // Constante 152
        public const int  g_const_153 = 113; // Constante 113
        public const int  g_const_154 = 154; // Constante 154
        public const int  g_const_155 = 155; // Constante 155


        public const int g_const_1000 = 1000; // Constante 1000
        public const int g_const_1001 = 1001; // Constante 1001
        public const int g_const_1003 = 1003; // Constante 1003
        public const int g_const_1005 = 1005; // Constante 1005
        public const int g_const_2000 = 2000; // Constante 2000
        public const int g_const_2001 = 2001; // Constante 2001
        public const int g_const_2002 = 2002; // Constante 2002
        public const int g_const_2003 = 2003; // Constante 2003
        public const int g_const_3000 = 3000; // Constante 3000
        public const int g_const_4000 = 4000; // Constante 4000
        public const int g_const_1020 = 1020; // Constante 1020
        
        public const int g_const_1021 = 1021; // Constante 1021

        public const string g_const_cultureEsp="es-PE"; //constante culture español

        public const string g_cost_nameTimeDefect= "America/Bogota"; // Constante timeStandar
        public const int g_const_length_numero_cpe_doc = 8; // Conteo de caracteres campo numero de serie de documento electrónico
        public const int g_const_length_id_cpe_doc = 15; // Conteo de caracteres campo ID de documento electrónico

        public const string g_const_rutaSufijo_xml = "XML/"; // ruta sufijo para xml
        public const string g_const_rutaSufijo_pdf = "PDF/"; // ruta sufijo para pdf
        public const string g_const_rutaSufijo_cdr = "CDR/"; // ruta sufijo para cdr
        public const string g_const_extension_xml = ".XML"; //  extensión para xml
        public const string g_const_extension_zip = ".ZIP"; //  extensión para zip
        public const string g_const_extension_pdf = ".PDF"; //  extensión para pdf
        public const string g_const_prefijoXmlCdr_respuesta = "R-"; // prefijo para xml y cdr respuesta



        public const string g_const_valExito = "Operación Exitosa"; // Constante de éxito para registro log fin
        public const string g_const_errlogCons = "Error al ejecutar método "; // Constante de error para registro log fin
        
        public const string g_const_error_interno = "Hubo un problema interno, por favor vuelva a intentar en unos momentos."; // Error interno general

        //errores

        public const string g_const_programa = "app_correo" ; // Nombre del Web Api
        public const string g_const_url = "CONSOLE CRON CORREO" ; // url de servicio
        public const string g_const_user_log= "SYS" ; // usuario de log
         public const string g_const_funcion_generarCpe= "app_correo.correo.CrearCorreosPendientes" ; // nombre método
         public const string g_const_funcion_generarOtrosCpe= "app_correo.correo.CrearCorreosOtroscpePendientes" ; // nombre método

            

         public const string g_const_funcion_generarCorreoErrores= "app_correo.correo.CrearCorreosErrorSistemas" ; // nombre método

        public const string g_const_funcion_enviarCorreoGuia = "app_correo.correo.CrearCorreosPendientesGuia"; //nombre de método de envio de correos guia
         public const string g_const_consultaexitosa = "CONSULTA EXITOSA"; // consulta exitosa
         public const string g_const_email_exito = "PROCESO DE CORREOS DE GUIA ENVIADOS CORRECTAMENTE"; // proceso de guia exito
         public const string g_const_email_fallido = "PROCESO DE CORREOS DE GUIA NO FUERON ENVIADOS CORRECATAMENTE"; // proceso de guia fallido
         public const string g_const_correo_notFound = "NO SE ENCONTRARON GUIAS PENDIENTES DE ENVIO DE CORREOS"; // lista de correos no encontrados
         public const string g_const_correo_empresa_notFound = "DATOS DE CORREO DE EMPRESA NO ENCONTRADO"; // datos de correo no encontrados

         public const string g_const_paramNoEnc = "Parametro no encontrado"; // datos de parametros no encontrados
         public const string g_const_empreNoEnc = "Datos de Empresa no encontrada"; // datos de empresa no encontrados
         public const string g_update_exito = "ACTUALIZACIÓN DE CORREO CORRECTAMENTE"; // exito de actualización de correo
         public const string g_const_email_destinatario_noTFound = "EMAIL DE DESTINATARIO NO ENCONTRADO"; // datos de destinatario

        public const string g_const_sufijoRutaTemporalPSE = "/PSEtmp"; //sufijo de ruta temporal
    }
}