/****************************************************************************************************************************************************************************************
 PROGRAMA: GenerarCorreo.cs
 VERSION : 1.0
 OBJETIVO: Clase generar comprobante
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;

using CEN;
using CLN;

using CAD;
using AppConfiguracion;
// using SpreadsheetLight;

// using Excel = Microsoft.Office.Interop.Excel;
// using System.Drawing;

// using GemBox.Spreadsheet;
using System.IO;

using Syncfusion.XlsIO;

using Syncfusion.Drawing;

namespace AppServicioCorreo
{
    public partial class GenerarCorreo
    {
        private string pClase = "GenerarCorreo"; //variable nombre de clase
        
        public GenerarCorreo()
        {
            //DESCRIPCION: CONSTRUCTOR GENERARCORREO          
            
        }

        public string getDateNowLima()
        {

            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);

            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(EN_Constante.g_const_formfech);

        }
        public string getDateHourNowLima()
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(EN_Constante.g_const_formhora);
        }

         public string getDateHourTmpNowLima()
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString("dd-MM-yyyy-HHmmss");
        }

        public void CrearCorreoEnvio()
        {
            //DESCRIPCION: FUNCION CREAR DOCUMENTO ELECTRONICO PARA FACTURA, BOLETA, NOTA DE CRÉDITO Y NOTÁ DE DÉBITO
            
            string rutabase, rutadoc;       // ruta base y ruta de documento
          
            EN_Documento oDocumento = new EN_Documento();   // clase entidad documento 
            NE_Correo oCorreo =  new NE_Correo(); // clase negocio correo
            List<EN_Documento> lstDoc = new List<EN_Documento>(); //lista de documentos
             EN_Empresa empresaDocumento;             // Entidad empresa
            {
               
                try
                {
                    lstDoc =  (List<EN_Documento>)oCorreo.listarDocsCorreo();
                    if (lstDoc.Count > 0)
                    {
                        foreach (var item in lstDoc.ToList())
                        {
                            empresaDocumento = new EN_Empresa();  
                            empresaDocumento = GetEmpresaById(item.Idempresa, item.Idpuntoventa, item.Id);

                            //rutas temporales S3
                            rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                            rutadoc = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                            if (!System.IO.Directory.Exists(rutadoc.Trim()))  System.IO.Directory.CreateDirectory(rutadoc.Trim());

                            //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
                            AppConfiguracion.FuncionesS3.InitialS3(empresaDocumento.Nrodocumento,empresaDocumento.Imagen);

                            var envCorreo= EnviarEmailComprobante(item,empresaDocumento, rutadoc , rutabase, item.Situacion)    ;
                            
                        }


                    }

                    
                  
                }
                catch (Exception ex)
                {
                 throw ex;  
                }
            }
           
        }

         public void CrearCorreoOtroscpeEnvio()
        {
            //DESCRIPCION: FUNCION CREAR DOCUMENTO ELECTRONICO PARA FACTURA, BOLETA, NOTA DE CRÉDITO Y NOTÁ DE DÉBITO
            
            string rutabase, rutadoc;       // ruta base y ruta de documento
          
            EN_OtrosCpe oDocumento = new EN_OtrosCpe();   // clase entidad documento 
            NE_Correo oCorreo =  new NE_Correo(); // clase negocio correo
            List<EN_OtrosCpe> lstDoc = new List<EN_OtrosCpe>(); //lista de documentos
             EN_Empresa empresaDocumento;             // Entidad empresa
            {
               
                try
                {
                    lstDoc =  (List<EN_OtrosCpe>)oCorreo.listarDocsOtroscpeCorreo();
                    if (lstDoc.Count > 0)
                    {
                        foreach (var item in lstDoc.ToList())
                        {
                            empresaDocumento = new EN_Empresa();  
                            empresaDocumento = GetEmpresaById(item.empresa_id, item.puntoventa_id, item.id);
                            //rutas temporales S3
                            rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                            rutadoc = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                            if (!System.IO.Directory.Exists(rutadoc.Trim()))  System.IO.Directory.CreateDirectory(rutadoc.Trim());

                            //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
                            AppConfiguracion.FuncionesS3.InitialS3(empresaDocumento.Nrodocumento,empresaDocumento.Imagen);

                            var envCorreo= EnviarEmailOtrosCpe(item,empresaDocumento, rutadoc , rutabase, item.situacion)    ;
                            
                        }


                    }

                    
                  
                }
                catch (Exception ex)
                {
                 throw ex;  
                }
            }
           
        }




         public void CrearCorreoEnvioErrores()
        {
            //DESCRIPCION: FUNCION CREAR DOCUMENTO ELECTRONICO PARA ERRORES AL CORREO DE SISTEMAS
            
          
            EN_Tbllog oDocumento = new EN_Tbllog();   // clase entidad documento 
            NE_Correo oCorreo =  new NE_Correo(); // clase negocio correo
            List<EN_Tbllog> lstDoc = new List<EN_Tbllog>(); //lista de documentos
             
            
               
                try
                {
                    lstDoc =  (List<EN_Tbllog>)oCorreo.listarDocsCorreoError();
                    if (lstDoc.Count > 0)
                    {
                       
                      
                            var envCorreo= EnviarEmailErrores(lstDoc)    ;
                    }
                  
                }
                catch (Exception ex)
                {
                 throw ex;  
                }
   
        }

        public EN_RespuestaData CrearCorreoEnvioGuia() 
        {
            EN_RespuestaData respuestaData = new EN_RespuestaData();
            EN_Guia objGuia = new EN_Guia();
            NE_Correo oCorreo =  new NE_Correo(); // clase negocio correo
            NE_Guia clnGuia = new NE_Guia();
             EN_RespuestaEmpresa respEmpresa = new EN_RespuestaEmpresa();
             EN_RespuestaEmpresa respParamEmpresa = new EN_RespuestaEmpresa();
            EN_RespuestaListaCorreoGuia respListCorreoGuia = new EN_RespuestaListaCorreoGuia();
            EN_EmpresaGuia empresaDocumento = new EN_EmpresaGuia();
            NE_Empresa objEmpresa = new NE_Empresa();
            string rutabase = string.Empty;
            string rutadoc = string.Empty;       // ruta base y ruta de documento
            try
            {
                respListCorreoGuia = clnGuia.listarCorreoGuia();
                respuestaData.ResplistaGuia = respListCorreoGuia.ResplistaGuia;

                if (respuestaData.ResplistaGuia.FlagVerificacion) 
                { 
                    foreach (var item in respListCorreoGuia.listDetGuia)
                    {
                        respEmpresa = GetEmpresaById2(item.empresa_id, item.puntoemision, item.id);
                        respuestaData.ResplistaGuia = respEmpresa.RespRegistro;
                        respuestaData.errorService = respEmpresa.errorService;
                            if (respuestaData.ResplistaGuia.FlagVerificacion) 
                            {
                                respParamEmpresa = objEmpresa.buscarParametroEmpresa(respEmpresa.objEmpresa.Id,EN_Constante.g_const_0);
                                respuestaData.ResplistaGuia = respParamEmpresa.RespRegistro;
                                respuestaData.errorService = respParamEmpresa.errorService;
                                if(respuestaData.ResplistaGuia.FlagVerificacion) {
                                    
                                    //rutas temporales S3
                                    rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                                    rutadoc = string.Format("{0}/{1}/", rutabase,  respEmpresa.objEmpresa.Nrodocumento);

                                    if (!System.IO.Directory.Exists(rutadoc.Trim()))  System.IO.Directory.CreateDirectory(rutadoc.Trim());

                                    //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
                                    AppConfiguracion.FuncionesS3.InitialS3( respEmpresa.objEmpresa.Nrodocumento, respEmpresa.objEmpresa.Imagen);



                                     respuestaData = EnviarEmailGuia(item,respEmpresa.objEmpresa,rutadoc , rutabase, item.situacion);
                                        
                                }


                            } 
                             
                        
                    }

                } else 
                {
                    if (respuestaData.ResplistaGuia.dato == 1000)
                    {
                        respuestaData.ResplistaGuia.FlagVerificacion = true;
                        respuestaData.errorService.TipoError = EN_Constante.g_const_0;
                        respuestaData.errorService.CodigoError = EN_Constante.g_const_2000;
                        respuestaData.errorService.DescripcionErr =EN_Constante.g_const_consultaexitosa;

                    }
                    if (respuestaData.ResplistaGuia.dato == 3000) 
                    {
                        respuestaData.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                        respuestaData.errorService.TipoError = EN_Constante.g_const_1;
                        respuestaData.errorService.CodigoError = EN_Constante.g_const_3000;
                        respuestaData.errorService.DescripcionErr =respuestaData.ResplistaGuia.DescRespuesta;

                    }
                }
                 
                return  respuestaData;
            }
            catch (Exception ex)
            {
                respuestaData.ResplistaGuia.FlagVerificacion = false;
                respuestaData.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                respuestaData.errorService.TipoError = EN_Constante.g_const_1;
                respuestaData.errorService.CodigoError = EN_Constante.g_const_3000;
                respuestaData.errorService.DescripcionErr = ex.Message;
                return respuestaData;

            }

        }

         public EN_Empresa GetEmpresaById(int idEmpresa, int Idpuntoventa, string documentoId)
        {
            EN_Empresa listaEmpresa;
            EN_Empresa oEmpresa = new EN_Empresa();
            NE_Empresa objEmpresa = new NE_Empresa();
            oEmpresa.Id = idEmpresa;
            oEmpresa.Nrodocumento = "";
            oEmpresa.RazonSocial = "";
            oEmpresa.Estado = "";
            listaEmpresa = (EN_Empresa)objEmpresa.listaEmpresa(oEmpresa, Idpuntoventa,  documentoId);
            return listaEmpresa;
        }
        public EN_RespuestaEmpresa GetEmpresaById2(int idEmpresa, int Idpuntoventa, string documentoId)
        {
           //  EN_EmpresaGuia listaEmpresa;
             EN_RespuestaEmpresa respEmpresa = new EN_RespuestaEmpresa();
            EN_EmpresaGuia oEmpresa = new EN_EmpresaGuia();
            NE_Empresa objEmpresa = new NE_Empresa();
            oEmpresa.Id = idEmpresa;
            oEmpresa.Nrodocumento = "";
            oEmpresa.RazonSocial = "";
            oEmpresa.Estado = "";
            respEmpresa = objEmpresa.listarEmpresaGuia(oEmpresa, Idpuntoventa);
            return respEmpresa;
        }


        public EN_Email buildEmail(int empresa_id)
        {
            var oEmail = new EN_Email();
            // Datos del remitente
            EN_Correo oCorreo = new EN_Correo();
            NE_Correo objCorreo = new NE_Correo();
            List<EN_Correo> listaCorreo;
            // NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
            //  EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
            // EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda


            try{


            oCorreo.id = 0;
            oCorreo.empresa_id = empresa_id;
            oCorreo.cuenta_email = "";
            oCorreo.UsuarioSession = "";
            oCorreo.CadenaAleatoria = "";
            oCorreo.es_default = "NO";
            listaCorreo = (List<EN_Correo> )objCorreo.listarCorreo(oCorreo);

            if (listaCorreo.Count > 0)
            {
                oEmail.CuentaDe = listaCorreo[0].cuenta_email;
                oEmail.NombreDe = listaCorreo[0].nombre_email;
                oEmail.ClaveDe = listaCorreo[0].clave_email;
                oEmail.CuentaSMTP = listaCorreo[0].cuenta_smtp;
                oEmail.PuertoSMTP = listaCorreo[0].puerto_smtp;
                oEmail.LinkDescarga = listaCorreo[0].link_descarga;
            }
            else
            {

                oCorreo.id = 0;
                oCorreo.empresa_id = 0;
                oCorreo.cuenta_email = "";
                oCorreo.UsuarioSession = "";
                oCorreo.CadenaAleatoria = "";
                oCorreo.es_default = "SI";
                listaCorreo = (List<EN_Correo> )objCorreo.listarCorreo(oCorreo);

                 if (listaCorreo.Count > 0)
                {

                oEmail.CuentaDe = listaCorreo[0].cuenta_email;
                oEmail.NombreDe = listaCorreo[0].nombre_email;
                oEmail.ClaveDe = listaCorreo[0].clave_email;
                oEmail.CuentaSMTP = listaCorreo[0].cuenta_smtp;
                oEmail.PuertoSMTP = listaCorreo[0].puerto_smtp;
                oEmail.LinkDescarga = listaCorreo[0].link_descarga;

                }
            
               
            }
            oEmail.Asunto = "";
            oEmail.Mensaje = "";
            return oEmail;

            
            }
            catch(Exception ex){
                throw ex;
            }


        }





// Enviar Email Comprobante
    public bool EnviarEmailComprobante(EN_Documento oDocumento, EN_Empresa empresaDocumento, string rutadocs, string rutalogo, string situacion)
    {
        string nombreArchivo=EN_Constante.g_const_vacio;        //nombre de archivo
        nombreArchivo= empresaDocumento.Nrodocumento.Trim() + "-" + oDocumento.Idtipodocumento.Trim() + "-" + oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim();
        try
        {
            var oEmail = buildEmail(empresaDocumento.Id);
            clsEmail clientEmail = new clsEmail();

            // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
            if ((!System.IO.Directory.Exists(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
                System.IO.Directory.CreateDirectory(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_xml);

            if ((!System.IO.Directory.Exists(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_pdf)))
                System.IO.Directory.CreateDirectory(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_pdf);

                if ((!System.IO.Directory.Exists(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
                System.IO.Directory.CreateDirectory(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_cdr);

            string rutaBaseS3Xml = string.Format("/{0}/{1}{2}",empresaDocumento.Nrodocumento.Trim(),EN_Constante.g_const_rutaSufijo_xml,nombreArchivo+EN_Constante.g_const_extension_xml);
            string rutaBaseS3Pdf = string.Format("/{0}/{1}{2}",empresaDocumento.Nrodocumento.Trim(),EN_Constante.g_const_rutaSufijo_pdf,nombreArchivo+EN_Constante.g_const_extension_pdf);
            string rutaBaseS3Cdr = string.Format("/{0}/{1}{2}",empresaDocumento.Nrodocumento.Trim(),EN_Constante.g_const_rutaSufijo_cdr,EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml);

            //extraemos si existe del s3 a local
            AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(rutaBaseS3Xml,rutadocs+EN_Constante.g_const_rutaSufijo_xml+nombreArchivo+EN_Constante.g_const_extension_xml).Wait(); 
            AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(rutaBaseS3Pdf,rutadocs+EN_Constante.g_const_rutaSufijo_pdf+nombreArchivo+EN_Constante.g_const_extension_pdf).Wait(); 
            AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(rutaBaseS3Pdf,rutadocs+EN_Constante.g_const_rutaSufijo_cdr+EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml).Wait(); 


            string logo = rutadocs + empresaDocumento.Imagen;
            string contenido;
            if (situacion == "A")
            {
                oEmail.Asunto = "La " + oDocumento.Tipodocumento + " Electrónica Nro. " + System.Convert.ToString(oDocumento.Serie + "-" + oDocumento.Numero) + " ACEPTADO POR SUNAT - " + empresaDocumento.RazonSocial;
                contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + oDocumento.Nombrecliente + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + oDocumento.Tipodocumento + " " + oDocumento.Serie + " " + oDocumento.Numero + " ha sido ACEPTADO POR SUNAT </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(oDocumento.Fecha).ToString("dd/MM/yyyy")  + "<br />" + "<b>Monto Total:  </b>" + String.Format(oDocumento.Importefinal.ToString(), "##,##0.00") + "<br />" + "<br />" + "Los comprobantes también pueden ser consultados en el link " + empresaDocumento.Web + ", ingresando mediante su usuario o utilizando la opción de Consulta Libre.<br />" + "<br />" + " <b>Atentamente.</b> " + "<br />" + " <b>Soporte Facturación Electrónica.</b> " + "<br />" + "<br />";
            }
            else if (situacion == "R")
            {
                oEmail.Asunto = "La " + oDocumento.Tipodocumento + " Electrónica Nro. " + System.Convert.ToString(oDocumento.Serie + "-" + oDocumento.Numero) + " RECHAZADO POR SUNAT - " + empresaDocumento.RazonSocial;
                contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + oDocumento.Nombrecliente + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + oDocumento.Tipodocumento + " " + oDocumento.Serie + " " + oDocumento.Numero + " ha sido RECHAZADO POR SUNAT </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(oDocumento.Fecha).ToString("dd/MM/yyyy")+ "<br />" + "<b>Monto Total:  </b>" + String.Format(oDocumento.Importefinal.ToString(), "##,##0.00") + "<br />" + "<br />" + "Los comprobantes también pueden ser consultados en el link " + empresaDocumento.Web + ", ingresando mediante su usuario o utilizando la opción de Consulta Libre.<br />" + "<br />" + " <b>Atentamente.</b> " + "<br />" + " <b>Soporte Facturación Electrónica.</b> " + "<br />" + "<br />";
            }
            else if (situacion == "B")
            {
                oEmail.Asunto = "La " + oDocumento.Tipodocumento + " Electrónica Nro. " + System.Convert.ToString(oDocumento.Serie + "-" + oDocumento.Numero) + " HA SIDO DE BAJA EN SUNAT - " + empresaDocumento.RazonSocial;
                contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + oDocumento.Nombrecliente + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + oDocumento.Tipodocumento + " " + oDocumento.Serie + " " + oDocumento.Numero + " ha sido DADO DE BAJA EN SUNAT </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(oDocumento.Fecha).ToString("dd/MM/yyyy")+ "<br />" + "<b>Monto Total:  </b>" + String.Format(oDocumento.Importefinal.ToString(), "##,##0.00") + "<br />" + "<br />" + "Los comprobantes también pueden ser consultados en el link " + empresaDocumento.Web + ", ingresando mediante su usuario o utilizando la opción de Consulta Libre.<br />" + "<br />" + " <b>Atentamente.</b> " + "<br />" + " <b>Soporte Facturación Electrónica.</b> " + "<br />" + "<br />";
            }
            else
            {
                oEmail.Asunto = "La " + oDocumento.Tipodocumento + " Electrónica Nro. " + System.Convert.ToString(oDocumento.Serie + "-" + oDocumento.Numero) + " HA SIDO GENERADA - " + empresaDocumento.RazonSocial;
                contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + oDocumento.Nombrecliente + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + oDocumento.Tipodocumento + " " + oDocumento.Serie + " " + oDocumento.Numero + " ha sido GENERADO </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(oDocumento.Fecha).ToString("dd/MM/yyyy") + "<br />" + "<b>Monto Total:  </b>" + String.Format(oDocumento.Importefinal.ToString(), "##,##0.00") + "<br />" + "<br />" + "Los comprobantes también pueden ser consultados en el link " + empresaDocumento.Web + ", ingresando mediante su usuario o utilizando la opción de Consulta Libre.<br />" + "<br />" + " <b>Atentamente.</b> " + "<br />" + " <b>Soporte Facturación Electrónica.</b> " + "<br />" + "<br />";
            }
            oEmail.Mensaje = contenido;
            if (oDocumento.Emailcliente != "")
            {
                int i = 0;
                foreach (var mailx in oDocumento.Emailcliente.Split(";"))
                    i = i + 1;
                string[] strPara = new string[i - 1 + 1];
                string[] strCC =null;
                int k = 0;
                foreach (var mailx in oDocumento.Emailcliente.Split(";"))
                {
                    strPara[k] = Strings.Trim(mailx);
                    k = k + 1;
                }
                if (situacion == "A")
                {
                    string[] strArchivoAdjunto = new string[3];
                    strArchivoAdjunto[0] = rutadocs+ EN_Constante.g_const_rutaSufijo_pdf + nombreArchivo + EN_Constante.g_const_extension_pdf;
                    strArchivoAdjunto[1] = rutadocs+ EN_Constante.g_const_rutaSufijo_xml + nombreArchivo + EN_Constante.g_const_extension_xml;
                    strArchivoAdjunto[2] = rutadocs+ EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombreArchivo + EN_Constante.g_const_extension_xml;
                    if (clientEmail.EnvioCorreo3(oEmail, strPara, strCC, strArchivoAdjunto, logo))
                    {
                        ActualizarEnvioMailFABVNCND(oDocumento, "SI");
                        return true;
                    }
                    else
                    {
                        ActualizarEnvioMailFABVNCND(oDocumento, "NO");
                        return false;
                    }
                }
                else
                {
                    string[] strArchivoAdjunto = new string[2];
                    strArchivoAdjunto[0] = rutadocs +   EN_Constante.g_const_rutaSufijo_pdf + nombreArchivo + EN_Constante.g_const_extension_pdf;
                    strArchivoAdjunto[1] = rutadocs +   EN_Constante.g_const_rutaSufijo_xml + nombreArchivo + EN_Constante.g_const_extension_xml;
                    if (clientEmail.EnvioCorreo3(oEmail, strPara, strCC, strArchivoAdjunto, logo))
                    {
                        ActualizarEnvioMailFABVNCND(oDocumento, "SI");
                        return true;
                    }
                    else
                    {
                        ActualizarEnvioMailFABVNCND(oDocumento, "NO");
                        return false;
                    }
                }
            }
            else
            {
                ActualizarEnvioMailFABVNCND(oDocumento, "NO");
                return false;
            }
            
        }
        catch (System.Exception ex)
        {
            
            throw ex;
        }
        finally
        {
             //Eliminamos los archivos copiados del s3
            if (File.Exists(rutadocs+EN_Constante.g_const_rutaSufijo_xml+nombreArchivo+EN_Constante.g_const_extension_xml))
                File.Delete(rutadocs+EN_Constante.g_const_rutaSufijo_xml+nombreArchivo+EN_Constante.g_const_extension_xml);
            
            if (File.Exists(rutadocs+EN_Constante.g_const_rutaSufijo_pdf+nombreArchivo+EN_Constante.g_const_extension_pdf))
                File.Delete(rutadocs+EN_Constante.g_const_rutaSufijo_pdf+nombreArchivo+EN_Constante.g_const_extension_pdf);

            if (File.Exists(rutadocs+EN_Constante.g_const_rutaSufijo_cdr+EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml))
                File.Delete(rutadocs+EN_Constante.g_const_rutaSufijo_cdr+EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml);
        
        }
        
    }

        
        
        public bool EnviarEmailOtrosCpe(EN_OtrosCpe oDocumento, EN_Empresa empresaDocumento, string rutadocs, string rutalogo, string situacion)
        {
            //DESCRIPCION: FUNCION PARA ENVIAR EMAIL DE OTROS CPE
               string nombreArchivo=EN_Constante.g_const_vacio;        //nombre de archivo
                nombreArchivo=  empresaDocumento.Nrodocumento.Trim() + "-" + oDocumento.tipodocumento_id.Trim() + "-" + oDocumento.serie.Trim() + "-" + oDocumento.numero.Trim();

            try
            {
                var oEmail = buildEmail(empresaDocumento.Id);
                clsEmail clientEmail = new clsEmail();

                // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
                if ((!System.IO.Directory.Exists(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
                    System.IO.Directory.CreateDirectory(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_xml);

                if ((!System.IO.Directory.Exists(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_pdf)))
                    System.IO.Directory.CreateDirectory(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_pdf);

                    if ((!System.IO.Directory.Exists(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
                    System.IO.Directory.CreateDirectory(rutadocs.Trim() + EN_Constante.g_const_rutaSufijo_cdr);

                string rutaBaseS3Xml = string.Format("/{0}/{1}{2}",empresaDocumento.Nrodocumento.Trim(),EN_Constante.g_const_rutaSufijo_xml,nombreArchivo+EN_Constante.g_const_extension_xml);
                string rutaBaseS3Pdf = string.Format("/{0}/{1}{2}",empresaDocumento.Nrodocumento.Trim(),EN_Constante.g_const_rutaSufijo_pdf,nombreArchivo+EN_Constante.g_const_extension_pdf);
                string rutaBaseS3Cdr = string.Format("/{0}/{1}{2}",empresaDocumento.Nrodocumento.Trim(),EN_Constante.g_const_rutaSufijo_cdr,EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml);

                //extraemos si existe del s3 a local
                AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(rutaBaseS3Xml,rutadocs+EN_Constante.g_const_rutaSufijo_xml+nombreArchivo+EN_Constante.g_const_extension_xml).Wait(); 
                AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(rutaBaseS3Pdf,rutadocs+EN_Constante.g_const_rutaSufijo_pdf+nombreArchivo+EN_Constante.g_const_extension_pdf).Wait(); 
                AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(rutaBaseS3Pdf,rutadocs+EN_Constante.g_const_rutaSufijo_cdr+EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml).Wait(); 

                string logo = rutadocs + empresaDocumento.Imagen;
                string contenido;
                if (situacion == "A")
                {
                    oEmail.Asunto = "La " + oDocumento.desctipodocumento + " Electrónica Nro. " + System.Convert.ToString(oDocumento.serie + "-" + oDocumento.numero) + " ACEPTADO POR SUNAT - " + empresaDocumento.RazonSocial;
                    contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + oDocumento.persona_nombrecomercial + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + oDocumento.desctipodocumento + " " + oDocumento.serie + " " + oDocumento.numero + " ha sido ACEPTADO POR SUNAT </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(oDocumento.fechaemision).ToString("dd/MM/yyyy")  + "<br />" + "<b>Monto Total:  </b>" + String.Format(oDocumento.importe_retper.ToString(), "##,##0.00") + "<br />" + "<br />" + "Los comprobantes también pueden ser consultados en el link " + empresaDocumento.Web + ", ingresando mediante su usuario o utilizando la opción de Consulta Libre.<br />" + "<br />" + " <b>Atentamente.</b> " + "<br />" + " <b>Soporte Facturación Electrónica.</b> " + "<br />" + "<br />";
                }
                else if (situacion == "R")
                {
                    oEmail.Asunto = "La " + oDocumento.desctipodocumento + " Electrónica Nro. " + System.Convert.ToString(oDocumento.serie + "-" + oDocumento.numero) + " RECHAZADO POR SUNAT - " + empresaDocumento.RazonSocial;
                    contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + oDocumento.persona_nombrecomercial + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + oDocumento.desctipodocumento + " " + oDocumento.serie + " " + oDocumento.numero + " ha sido RECHAZADO POR SUNAT </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(oDocumento.fechaemision).ToString("dd/MM/yyyy")+ "<br />" + "<b>Monto Total:  </b>" + String.Format(oDocumento.importe_retper.ToString(), "##,##0.00") + "<br />" + "<br />" + "Los comprobantes también pueden ser consultados en el link " + empresaDocumento.Web + ", ingresando mediante su usuario o utilizando la opción de Consulta Libre.<br />" + "<br />" + " <b>Atentamente.</b> " + "<br />" + " <b>Soporte Facturación Electrónica.</b> " + "<br />" + "<br />";
                }
                else if (situacion == "B")
                {
                    oEmail.Asunto = "La " + oDocumento.desctipodocumento + " Electrónica Nro. " + System.Convert.ToString(oDocumento.serie + "-" + oDocumento.numero) + " HA SIDO DADA DE BAJA EN SUNAT - " + empresaDocumento.RazonSocial;
                    contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + oDocumento.persona_nombrecomercial + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + oDocumento.desctipodocumento + " " + oDocumento.serie + " " + oDocumento.numero + " ha sido DADO DE BAJA EN SUNAT </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(oDocumento.fechaemision).ToString("dd/MM/yyyy")+ "<br />" + "<b>Monto Total:  </b>" + String.Format(oDocumento.importe_retper.ToString(), "##,##0.00") + "<br />" + "<br />" + "Los comprobantes también pueden ser consultados en el link " + empresaDocumento.Web + ", ingresando mediante su usuario o utilizando la opción de Consulta Libre.<br />" + "<br />" + " <b>Atentamente.</b> " + "<br />" + " <b>Soporte Facturación Electrónica.</b> " + "<br />" + "<br />";
                }
                else
                {
                    oEmail.Asunto = "La " + oDocumento.desctipodocumento + " Electrónica Nro. " + System.Convert.ToString(oDocumento.serie + "-" + oDocumento.numero) + " HA SIDO GENERADA - " + empresaDocumento.RazonSocial;
                    contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + oDocumento.persona_nombrecomercial + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + oDocumento.desctipodocumento + " " + oDocumento.serie + " " + oDocumento.numero + " ha sido GENERADO </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(oDocumento.fechaemision).ToString("dd/MM/yyyy") + "<br />" + "<b>Monto Total:  </b>" + String.Format(oDocumento.importe_retper.ToString(), "##,##0.00") + "<br />" + "<br />" + "Los comprobantes también pueden ser consultados en el link " + empresaDocumento.Web + ", ingresando mediante su usuario o utilizando la opción de Consulta Libre.<br />" + "<br />" + " <b>Atentamente.</b> " + "<br />" + " <b>Soporte Facturación Electrónica.</b> " + "<br />" + "<br />";
                }
                oEmail.Mensaje = contenido;
                if (oDocumento.persona_email != "")
                {
                    int i = 0;
                    foreach (var mailx in oDocumento.persona_email.Split(";"))
                        i = i + 1;
                    string[] strPara = new string[i - 1 + 1];
                    string[] strCC =null;
                    int k = 0;
                    foreach (var mailx in oDocumento.persona_email.Split(";"))
                    {
                        strPara[k] = Strings.Trim(mailx);
                        k = k + 1;
                    }
                    if (situacion == "A")
                    {
                        string[] strArchivoAdjunto = new string[3];
                        strArchivoAdjunto[0] = rutadocs+ EN_Constante.g_const_rutaSufijo_pdf + nombreArchivo + EN_Constante.g_const_extension_pdf;
                        strArchivoAdjunto[1] = rutadocs+ EN_Constante.g_const_rutaSufijo_xml + nombreArchivo + EN_Constante.g_const_extension_xml;
                        strArchivoAdjunto[2] = rutadocs+ EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombreArchivo + EN_Constante.g_const_extension_xml;
                        
                        if (clientEmail.EnvioCorreo3(oEmail, strPara, strCC, strArchivoAdjunto, logo))
                        {
                            ActualizarEnvioMailOtroscpe(oDocumento, "SI");
                            return true;
                        }
                        else
                        {
                            ActualizarEnvioMailOtroscpe(oDocumento, "NO");
                            return false;
                        }
                    }
                    else
                    {
                        string[] strArchivoAdjunto = new string[2];
                        strArchivoAdjunto[0] = rutadocs +   EN_Constante.g_const_rutaSufijo_pdf + nombreArchivo + EN_Constante.g_const_extension_pdf;
                        strArchivoAdjunto[1] = rutadocs +   EN_Constante.g_const_rutaSufijo_xml + nombreArchivo + EN_Constante.g_const_extension_xml;

                        if (clientEmail.EnvioCorreo3(oEmail, strPara, strCC, strArchivoAdjunto, logo))
                        {
                            ActualizarEnvioMailOtroscpe(oDocumento, "SI");
                            return true;
                        }
                        else
                        {
                            ActualizarEnvioMailOtroscpe(oDocumento, "NO");
                            return false;
                        }
                    }
                }
                else
                {
                    ActualizarEnvioMailOtroscpe(oDocumento, "NO");
                    return false;
                }
                
            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                 //Eliminamos los archivos copiados del s3
                if (File.Exists(rutadocs+EN_Constante.g_const_rutaSufijo_xml+nombreArchivo+EN_Constante.g_const_extension_xml))
                    File.Delete(rutadocs+EN_Constante.g_const_rutaSufijo_xml+nombreArchivo+EN_Constante.g_const_extension_xml);
                
                if (File.Exists(rutadocs+EN_Constante.g_const_rutaSufijo_pdf+nombreArchivo+EN_Constante.g_const_extension_pdf))
                    File.Delete(rutadocs+EN_Constante.g_const_rutaSufijo_pdf+nombreArchivo+EN_Constante.g_const_extension_pdf);

                if (File.Exists(rutadocs+EN_Constante.g_const_rutaSufijo_cdr+EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml))
                    File.Delete(rutadocs+EN_Constante.g_const_rutaSufijo_cdr+EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml);

            }
            
        }



        public EN_RespuestaData EnviarEmailGuia(EN_Guia objGuia,EN_EmpresaGuia empresaDocumento,string rutadoc, string rutalogo,string situacion) 
        {
            
            EN_RespuestaData rspuestaEmail = new EN_RespuestaData();
            clsEmail clientEmail = new clsEmail();
            NE_Guia guia = new NE_Guia();
            EN_RespuestaData resData = new EN_RespuestaData();       
            string logo = string.Empty;
            string contenido = string.Empty;

            string nombreArchivo=EN_Constante.g_const_vacio;        //nombre de archivo
            
            nombreArchivo= empresaDocumento.Nrodocumento.Trim() + "-" + objGuia.tipodocumento_id.Trim() + "-" + objGuia.serie.Trim() + "-" + objGuia.numero.Trim();

            try
            {
                 // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
                if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
                    System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml);

                if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf)))
                    System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf);

                    if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
                    System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr);

                string rutaBaseS3Xml = string.Format("/{0}/{1}{2}",empresaDocumento.Nrodocumento.Trim(),EN_Constante.g_const_rutaSufijo_xml,nombreArchivo+EN_Constante.g_const_extension_xml);
                string rutaBaseS3Pdf = string.Format("/{0}/{1}{2}",empresaDocumento.Nrodocumento.Trim(),EN_Constante.g_const_rutaSufijo_pdf,nombreArchivo+EN_Constante.g_const_extension_pdf);
                string rutaBaseS3Cdr = string.Format("/{0}/{1}{2}",empresaDocumento.Nrodocumento.Trim(),EN_Constante.g_const_rutaSufijo_cdr,EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml);

                //extraemos si existe del s3 a local
                AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(rutaBaseS3Xml,rutadoc+EN_Constante.g_const_rutaSufijo_xml+nombreArchivo+EN_Constante.g_const_extension_xml).Wait(); 
                AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(rutaBaseS3Pdf,rutadoc+EN_Constante.g_const_rutaSufijo_pdf+nombreArchivo+EN_Constante.g_const_extension_pdf).Wait(); 
                AppConfiguracion.FuncionesS3.verificaLocalExtraeS3Async(rutaBaseS3Pdf,rutadoc+EN_Constante.g_const_rutaSufijo_cdr+EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml).Wait(); 



                var oEmail = buildEmail(empresaDocumento.Id);
                logo = rutadoc + empresaDocumento.Imagen;
                if (situacion.ToUpper() == "A")
                {
                oEmail.Asunto = "La " + objGuia.tipodocumento + " Electrónica Nro. " + System.Convert.ToString(objGuia.serie + "-" + objGuia.numero) + " ACEPTADO POR SUNAT - " + empresaDocumento.RazonSocial;
                contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + objGuia.destinatario_nombre + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + objGuia.tipodocumento + " " + objGuia.serie+ " " + objGuia.numero + " ha sido ACEPTADO POR SUNAT </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(objGuia.fechaemision).ToString("dd/MM/yyyy")  + "<br />";

                } else if (situacion.ToUpper() == "B")
                {
                    oEmail.Asunto = "La " + objGuia.tipodocumento + " Electrónica Nro. " + System.Convert.ToString(objGuia.serie + "-" + objGuia.numero) + " HA SIDO DE BAJA EN SUNAT - " + empresaDocumento.RazonSocial;
                    contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + objGuia.destinatario_nombre+ "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + objGuia.tipodocumento  + " " + objGuia.serie + " " + objGuia.numero + " ha sido DADO DE BAJA EN SUNAT </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(objGuia.fechaemision).ToString("dd/MM/yyyy")+ "<br />";
                } else if (situacion.ToUpper() == "R")
                {
                    oEmail.Asunto = "La " + objGuia.tipodocumento  + " Electrónica Nro. " + System.Convert.ToString(objGuia.serie + "-" + objGuia.numero) + " RECHAZADO POR SUNAT - " + empresaDocumento.RazonSocial;
                    contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + objGuia.destinatario_nombre + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + objGuia.tipodocumento + " " + objGuia.serie  + " " + objGuia.numero  + " ha sido RECHAZADO POR SUNAT </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(objGuia.fechaemision).ToString("dd/MM/yyyy")+ "<br />";
                } else
                {
                    oEmail.Asunto = "La " + objGuia.tipodocumento + " Electrónica Nro. " + System.Convert.ToString(objGuia.serie + "-" + objGuia.numero) + " HA SIDO GENERADA - " + empresaDocumento.RazonSocial;
                    contenido = "<br />" + "<b><u>Estimado(a),</u></b>" + "<br />" + "  " + "<br />" + "<b>" + objGuia.destinatario_nombre + "  </b>" + "<br />" + "Por encargo del emisor " + empresaDocumento.RazonSocial + " nos es grato informar que el documento electrónico: <b>" + objGuia.tipodocumento + " " + objGuia.serie + " " + objGuia.numero + " ha sido GENERADO </b> con los siguientes datos:" + "<br />" + "  " + "<br />" + "<b>N° RUC del Emisor:  </b>" + empresaDocumento.Nrodocumento + "<br />" + "<b>Razon Social:  </b>" + empresaDocumento.RazonSocial + "<br />" + "<b>Fecha Emisión:  </b>" + Convert.ToDateTime(objGuia.fechaemision).ToString("dd/MM/yyyy") + "<br />";
                }
            oEmail.Mensaje = contenido;
            if (objGuia.destinatario_email!= "")
            {
                int i = 0;
                foreach (var mailx in objGuia.destinatario_email.Split(";"))
                    i = i + 1;
                string[] strPara = new string[i - 1 + 1];
                string[] strCC =null;
                int k = 0;
                foreach (var mailx in objGuia.destinatario_email.Split(";"))
                {
                    strPara[k] = Strings.Trim(mailx);
                    k = k + 1;
                }
                if (situacion.ToUpper() == "A")
                {
                    string[] strArchivoAdjunto = new string[3];
                    strArchivoAdjunto[0] = rutadoc+ EN_Constante.g_const_rutaSufijo_pdf + nombreArchivo + EN_Constante.g_const_extension_pdf;
                    strArchivoAdjunto[1] = rutadoc+ EN_Constante.g_const_rutaSufijo_xml + nombreArchivo + EN_Constante.g_const_extension_xml;
                    strArchivoAdjunto[2] = rutadoc+ EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombreArchivo + EN_Constante.g_const_extension_xml;
                    if (clientEmail.EnvioCorreo3(oEmail, strPara, strCC, strArchivoAdjunto, logo))
                    {
                    // ActualizarEnvioMailFABVNCND(oDocumento, "SI");
                    rspuestaEmail = guia.actualizarEnvioEmail(objGuia,"SI");    
                    return rspuestaEmail;         
                    }
                    else
                    {
                        rspuestaEmail = guia.actualizarEnvioEmail(objGuia,"NO");
                        return rspuestaEmail;  
                    }
                } else
                {
                    string[] strArchivoAdjunto = new string[2];
                    strArchivoAdjunto[0] = rutadoc +   EN_Constante.g_const_rutaSufijo_pdf + nombreArchivo + EN_Constante.g_const_extension_pdf;
                    strArchivoAdjunto[1] = rutadoc +   EN_Constante.g_const_rutaSufijo_xml + nombreArchivo + EN_Constante.g_const_extension_xml;
                    if (clientEmail.EnvioCorreo3(oEmail, strPara, strCC, strArchivoAdjunto, logo))
                    {
    
                    rspuestaEmail = guia.actualizarEnvioEmail(objGuia,"SI");    
                    return rspuestaEmail;         
                    }
                    else
                    {
                        rspuestaEmail = guia.actualizarEnvioEmail(objGuia,"NO");
                        return rspuestaEmail;  
                    }

                }

            } else {
                rspuestaEmail = guia.actualizarEnvioEmail(objGuia,"NO");
                return rspuestaEmail;
            }
            }
            catch (Exception ex)
            {
                
                rspuestaEmail.ResplistaGuia.FlagVerificacion = false;
                rspuestaEmail.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                rspuestaEmail.errorService.TipoError = EN_Constante.g_const_1;
                rspuestaEmail.errorService.DescripcionErr  = ex.Message;
                rspuestaEmail.errorService.CodigoError = EN_Constante.g_const_3000;
                return rspuestaEmail;
            }
            finally
            {
                //Eliminamos los archivos copiados del s3
                if (File.Exists(rutadoc+EN_Constante.g_const_rutaSufijo_xml+nombreArchivo+EN_Constante.g_const_extension_xml))
                    File.Delete(rutadoc+EN_Constante.g_const_rutaSufijo_xml+nombreArchivo+EN_Constante.g_const_extension_xml);
                
                if (File.Exists(rutadoc+EN_Constante.g_const_rutaSufijo_pdf+nombreArchivo+EN_Constante.g_const_extension_pdf))
                    File.Delete(rutadoc+EN_Constante.g_const_rutaSufijo_pdf+nombreArchivo+EN_Constante.g_const_extension_pdf);

                if (File.Exists(rutadoc+EN_Constante.g_const_rutaSufijo_cdr+EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml))
                    File.Delete(rutadoc+EN_Constante.g_const_rutaSufijo_cdr+EN_Constante.g_const_prefijoXmlCdr_respuesta+nombreArchivo+EN_Constante.g_const_extension_xml);
            
            }
        }

    

// Enviar Email Comprobante
    public bool EnviarEmailErrores(List<EN_Tbllog> listaDoc)
    {
        NE_Proceso objProc = new NE_Proceso();                  //clase negocio proceso
        EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
        EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
        
       
        clsEmail clientEmail = new clsEmail();
        string contenido;
        string licenciaSyncfusion="", Emailcliente="", origenerror="", tipo , servidor, nivel;
          
        string pathFile2 = AppDomain.CurrentDomain.BaseDirectory + "errores-pse-"+getDateHourTmpNowLima()+".xlsx";
        try
        {
           

            bus_par.par_conceptopfij=EN_Constante.g_const_1;
            bus_par.par_conceptocorr=EN_Constante.g_const_21;
            res_par =objProc.buscar_tablaParametro(bus_par);
            licenciaSyncfusion = res_par.par_descripcion;

            bus_par.par_conceptopfij=EN_Constante.g_const_1;
            bus_par.par_conceptocorr=EN_Constante.g_const_20;
            res_par =objProc.buscar_tablaParametro(bus_par);
            Emailcliente= res_par.par_descripcion;

            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(licenciaSyncfusion);
            using (ExcelEngine excelEngine = new ExcelEngine())
            {       

                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;

                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];

                //Disable gridlines in the worksheet
                worksheet.IsGridLinesVisible = true;

               
                    //Merge cells
                    worksheet.Range["A1:C2"].Merge();

                    //Enter text to the cell D1 and apply formatting.
                    worksheet.Range["A1"].Text = "ERRORES PSE";
                    worksheet.Range["A1"].CellStyle.Font.Bold = true;
                    worksheet.Range["A1"].CellStyle.Font.RGBColor = Color.FromArgb(42, 118, 189);
                    worksheet.Range["A1"].CellStyle.Font.Size = 24;

                    worksheet.Range["A1"].CellStyle.HorizontalAlignment = ExcelHAlign.HAlignLeft;
                    worksheet.Range["A1"].CellStyle.VerticalAlignment = ExcelVAlign.VAlignTop;

                    worksheet.Range["A4"].Text = "FECHA";
                    worksheet.Range["B4"].Text = "HORA";
                    worksheet.Range["A5"].Text = getDateNowLima();
                    worksheet.Range["B5"].Text = getDateHourNowLima();

                    

                    worksheet.Range["A4:B4"].CellStyle.Color = Color.FromArgb(42, 118, 189);

                    worksheet.Range["A4:B4"].CellStyle.Font.Color = ExcelKnownColors.White;

                    worksheet.Range["A4:B4"].CellStyle.Font.Bold = true;

                    worksheet.Range["A4:B5"].CellStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
                    worksheet.Range["A4:B5"].CellStyle.VerticalAlignment = ExcelVAlign.VAlignCenter;

                    worksheet.Range[7, 1].Value=  "Codigo";
                    worksheet.Range[7, 2].Value="Programa";
                    worksheet.Range[7, 3].Value="Id_documento";
                    worksheet.Range[7, 4].Value="Tipo_documento" ;
                    worksheet.Range[7, 5].Value="Id_empresa";
                    worksheet.Range[7, 6].Value="Id_punto_venta";
                    worksheet.Range[7, 7].Value="Nivel_ejecucion";
                    worksheet.Range[7, 8].Value="Nombre_funcion";
                    worksheet.Range[7, 9].Value="Url";
                    worksheet.Range[7, 10].Value="Tipo_servidor";
                    worksheet.Range[7, 11].Value="Tipo";
                    worksheet.Range[7, 12].Value="Origen_error";
                    worksheet.Range[7, 13].Value="Error";
                    worksheet.Range[7, 14].Value="Usuario";
                    worksheet.Range[7, 15].Value="Fecha_proceso";
                    worksheet.Range[7, 16].Value="Hora_inicio_proceso";
                    worksheet.Range[7, 17].Value="Hora_fin_proceso";
                    worksheet.Range[7, 18].Value="Datos_entrada";
                    worksheet.Range[7, 19].Value="Datos_salida";

                    worksheet.Range[7,1,7,19].CellStyle.Color = Color.FromArgb(42, 118, 189);

                    worksheet.Range[7,1,7,19].CellStyle.Font.Color = ExcelKnownColors.White;


                     int x=8;
                    foreach (var oDocumento in listaDoc.ToList())
                    {

                        switch (oDocumento.logoerr.ToString())
                        {
                            case "1":
                                origenerror="Servidor Generar Comprobante";
                                break;
                            case "2":
                                origenerror="Servidor ServiceDoc";
                                break;
                            case "3":
                                origenerror="Servidor ServiceGuia";
                                break;
                            case "4":
                                origenerror="Servidor ServiceOtrosCpe";
                                break;
                            case "5":
                                origenerror="ervidor consola";
                                break;
                            case "6":
                                origenerror="Servidor correo";
                                break;
                            case "7":
                                origenerror="Servidor webapi consultaweb comprobante";
                                break;

                        }

                        tipo=  (oDocumento.logtipo.ToString()=="1")?"Invocacion":"Respuesta";
                        servidor= (oDocumento.logtser.ToString()=="1")?"Primario":"Secundario";
                        nivel = (oDocumento.lognive.ToString()=="1")?"Servidor":"Cliente";
                    

                            worksheet.Range[x, 1].Value=oDocumento.logcodi.ToString();
                            worksheet.Range[x, 2].Value=oDocumento.logprog.ToString();
                            worksheet.Range[x, 3].Value=oDocumento.logidoc.ToString();
                            worksheet.Range[x, 4].Value=oDocumento.logtdoc.ToString();
                            worksheet.Range[x, 5].Value=oDocumento.logiemp.ToString();
                            worksheet.Range[x, 6].Value=oDocumento.logipvt.ToString();
                            worksheet.Range[x, 7].Value=nivel;
                            worksheet.Range[x, 8].Value=oDocumento.lognwba.ToString();
                            worksheet.Range[x, 9].Value=oDocumento.logdurl.ToString();
                            worksheet.Range[x, 10].Value=servidor;
                            worksheet.Range[x, 11].Value=tipo;
                            worksheet.Range[x, 12].Value=origenerror;
                            worksheet.Range[x, 13].Value=oDocumento.logerro.ToString();
                            worksheet.Range[x, 14].Value=oDocumento.loguser.ToString();
                            worksheet.Range[x, 15].Value=oDocumento.logfech.ToString();
                            worksheet.Range[x, 16].Value=oDocumento.loghini.ToString();
                            worksheet.Range[x, 17].Value=oDocumento.loghfin.ToString();
                            worksheet.Range[x, 18].Value=oDocumento.logdate.ToString();
                            worksheet.Range[x, 19].Value=oDocumento.logdats.ToString();

                            x=x+1;
                    }

                    worksheet.Range[7,1,x,19].AutofitColumns();
                    worksheet.Range["A5"].AutofitColumns();



                    
                    //Saving the Excel to the MemoryStream 
                    MemoryStream stream = new MemoryStream();
                    workbook.SaveAs(stream);

                    //Set the position as '0'.
                    stream.Position = 0;

                    using(var fileStream =new FileStream(pathFile2, FileMode.Create))
                    {

                        stream.CopyTo(fileStream);
                    }

            }


     

            var oEmail = buildEmail(EN_Constante.g_const_0);

            oEmail.Asunto = "Aviso de caída [Envío Automático] "+getDateNowLima()+" "+getDateHourNowLima();
            contenido = "<br />" + "<b><u>Estimado(a)</u></b>" + "<br />" + "  " 
            + "<br />" + "<b> Servicio Técnico  </b>" + "<br />" 
            + "Se le informa la lista de caídas detallada en el excel adjunto."+ "<br />" ;
            
            oEmail.Mensaje = contenido;
        

            string[] strArchivoAdjunto = new string[1];
            strArchivoAdjunto[0] = pathFile2;

     
            if (Emailcliente != "")
            {
                int i = 0;
                foreach (var mailx in Emailcliente.Split(";"))
                    i = i + 1;
                string[] strPara = new string[i - 1 + 1];
                string[] strCC =null;
                int k = 0;
                foreach (var mailx in Emailcliente.Split(";"))
                {
                    strPara[k] = Strings.Trim(mailx);
                    k = k + 1;
                }

                    if (clientEmail.EnvioCorreoError(oEmail, strPara, strCC, strArchivoAdjunto))
                    {
                        foreach (var oDocumento in listaDoc.ToList())
                        {
                            ActualizarEnvioMailError(oDocumento, "SI");
                        }
                    
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                
            }
            return true;

         }
        catch (System.Exception ex)
        {
            
            throw ex;
        }
        finally{
            if(File.Exists(pathFile2))
            {
                File.Delete(pathFile2);
            }
        }
       
    }

        public void ActualizarEnvioMailError(EN_Tbllog oDocumento, string estado)
        {
            NE_Facturacion objFacturacion = new NE_Facturacion();
            try
            {
                objFacturacion.ActualizarEnvioError(oDocumento, estado);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void ActualizarEnvioMailFABVNCND(EN_Documento oDocumento, string estado)
        {
            NE_Facturacion objFacturacion = new NE_Facturacion();
            try
            {
                objFacturacion.ActualizarEnvioMail(oDocumento, estado);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

           public void ActualizarEnvioMailOtroscpe(EN_OtrosCpe oDocumento, string estado)
        {
            NE_Facturacion objFacturacion = new NE_Facturacion();
            try
            {
                objFacturacion.ActualizarEnvioMailOtroscpe(oDocumento, estado);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
    }

}