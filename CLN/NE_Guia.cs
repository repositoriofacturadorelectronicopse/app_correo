using CAD;
using CEN;
using System;
namespace CLN
{
    public class NE_Guia
    {
        public EN_RespuestaListaCorreoGuia listarCorreoGuia() 
        {
            EN_RespuestaListaCorreoGuia respuesta = new EN_RespuestaListaCorreoGuia();
            AD_Guia objGuia = new AD_Guia();
            try
            {
                respuesta = objGuia.listarCorreoGuia();
                return respuesta;
                 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }


        }
        public EN_RespuestaListaCorreo listarCorreoGuia(EN_Correo pCorreo)
        {
            EN_RespuestaListaCorreo respuestaCorreo = new EN_RespuestaListaCorreo();
            AD_Guia objGuia = new AD_Guia();
            try
            {
                respuestaCorreo = objGuia.listarCorreoGuia(pCorreo);
                return respuestaCorreo;
                 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }


        }
        public EN_RespuestaData actualizarEnvioEmail(EN_Guia Guia,string estado) 
        {
            EN_RespuestaData resData = new EN_RespuestaData();
            AD_Guia objGuia = new AD_Guia();
            try
            {
                resData = objGuia.actualizarEnvioEmail(Guia,estado);
                return resData;
                 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }
        
    }
}