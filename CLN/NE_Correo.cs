using System;
using CAD;
using CEN;

namespace CLN
{
   public class NE_Correo
{
    private string pClase = "NE_Correo";

    public object listarDocsCorreo()
    {
        try
        {
            AD_Correo obj = new AD_Correo();
            return obj.listarDocsCorreo();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

     public object listarDocsOtroscpeCorreo()
    {
        try
        {
            AD_Correo obj = new AD_Correo();
            return obj.listarDocsOtroscpeCorreo();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


       public object listarDocsCorreoError()
    {
        try
        {
            AD_Correo obj = new AD_Correo();
            return obj.listarDocsCorreoError();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public object listarCorreo(EN_Correo pCorreo)
    {
        try
        {
            AD_Correo obj = new AD_Correo();
            return obj.listarCorreo(pCorreo);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
}
}