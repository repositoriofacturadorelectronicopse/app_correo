﻿using System;
using CAD;
using CEN;

namespace CLN
{
  

    public partial class NE_Facturacion
    {
        private string pClase = "NE_Facturacion";

        public string buscarParametroAppSettings(int codconcepto,int codcorrelativo)
        {

                   try
            {
                var obj = new AD_Facturacion();
                return obj.buscarParametroAppSettings(codconcepto,codcorrelativo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string buscarConstantesTablaSunat(string codtabla,string codalterno)
        {

                   try
            {
                var obj = new AD_Facturacion();
                return obj.buscarConstantesTablaSunat(codtabla,codalterno);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string ObtenerCodigoSunat(string codtabla, string codigo)
        {
            try
            {
                var obj = new AD_Facturacion();
                return obj.ObtenerCodigoSunat(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerDescripcionPorCodElemento(string codtabla, string codigo)
        {
            try
            {
                var obj = new AD_Facturacion();
                return obj.ObtenerDescripcionPorCodElemento(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     

        public string ObtenerCodigoAlternoPorCodigoSistema(string codtabla, string codigo)
        {
            try
            {
                var obj = new AD_Facturacion();
                return obj.ObtenerCodigoAlternoPorCodigoSistema(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     

    

      
    
        


   

  

    public void ActualizarEnvioMail(EN_Documento oDocumento, string estado)
    {
        try
        {
            AD_Facturacion obj = new AD_Facturacion();
            obj.ActualizarEnvioMail(oDocumento, estado);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void ActualizarEnvioMailOtroscpe(EN_OtrosCpe oDocumento, string estado)
    {
        try
        {
            AD_Facturacion obj = new AD_Facturacion();
            obj.ActualizarEnvioMailOtroscpe(oDocumento, estado);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

      public void ActualizarEnvioError(EN_Tbllog oDocumento, string estado)
    {
        try
        {
            AD_Facturacion obj = new AD_Facturacion();
            obj.ActualizarEnvioError(oDocumento, estado);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


  

    }
}
