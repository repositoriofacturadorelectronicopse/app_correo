﻿using System;
using CAD;
using CEN;


namespace CLN
{
    public class NE_Empresa
    {

        private string pClase = "NE_Empresa";

        public object listaEmpresa(EN_Empresa pEmpresa, int Idpuntoventa, string documentoId)
        {
            try
            {
                AD_Empresa obj = new AD_Empresa();
                return obj.listarEmpresa(pEmpresa, Idpuntoventa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public EN_RespuestaEmpresa listarEmpresaGuia(EN_EmpresaGuia pEmpresa, int IdpuntoEmision)
        {
            EN_RespuestaEmpresa respuesta = new EN_RespuestaEmpresa();
            AD_Empresa empresa = new AD_Empresa();
            try
            {
                 respuesta = empresa.listarEmpresaGuia(pEmpresa,IdpuntoEmision);
                 return respuesta;

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public EN_RespuestaEmpresa buscarParametroEmpresa(int codconcepto, int codcorrelativo)
        {
            EN_RespuestaEmpresa respuesta = new EN_RespuestaEmpresa();
            AD_Empresa empresa = new AD_Empresa();
            try
            {
                 respuesta = empresa.buscarParametroEmpresa(codconcepto,codcorrelativo);
                 return respuesta;

            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }
     
   

    }
}
