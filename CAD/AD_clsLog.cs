﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_clsLog.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos Log
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections;

using CEN;
using System.Data.SqlClient;
using System.Data;

namespace CAD
{


    public partial class AD_clsLog
    {
        private readonly AD_Cado _datosConexion;  // Clase Cado
        private SqlConnection _sqlConexion; // Cadena de conexión SQL
        private readonly SqlCommand _sqlCommand;    // cariable Command de SQL
    

        public AD_clsLog()
        {
            //DESCRIPCION: CLASE CONSTRUCTOR
        }

       
        public Int32 registrar_log_inicio(string p_prog,string p_idoc,  string p_tdoc,int p_iemp,int p_ipvt,string p_nemp, int p_nive,
                                        string p_nwba, string p_durl, int p_tser,int p_tipo,int p_ferr,  int p_oerr, string p_erro,string p_derr,string p_fech,
                                        string p_hini, string p_date, string p_hfin, string p_dats,string p_user)
        {
            //DESCIPCION: función para registrar log inicio
            AD_Cado cado = new AD_Cado();   // clase Conexión
            SqlDataReader dr; // Data Reader
            Int32 ntraLog = 0; // Número de transacción
            try
            {
                
                    using( _sqlConexion= new SqlConnection(cado.CadenaConexion()))
                    {

                    
                    _sqlConexion.Open();
                    using(SqlCommand cmd = new SqlCommand("Usp_registrar_log_inicio_webapi", _sqlConexion))
                    {
                        cmd.CommandType= CommandType.StoredProcedure;

                        cmd.Parameters.Add("@p_prog", SqlDbType.VarChar).Value= p_prog  ;
                        cmd.Parameters.Add("@p_idoc", SqlDbType.VarChar).Value= p_idoc  ;
                        cmd.Parameters.Add("@p_tdoc", SqlDbType.VarChar).Value= p_tdoc  ;
                        cmd.Parameters.Add("@p_iemp", SqlDbType.SmallInt).Value= p_iemp;
                        cmd.Parameters.Add("@p_ipvt", SqlDbType.VarChar).Value= p_ipvt  ;
                        cmd.Parameters.Add("@p_nemp", SqlDbType.VarChar).Value= p_nemp  ;
                        cmd.Parameters.Add("@p_nive", SqlDbType.SmallInt).Value= p_nive  ;
                        cmd.Parameters.Add("@p_nwba", SqlDbType.VarChar).Value=  p_nwba ;
                        cmd.Parameters.Add("@p_durl", SqlDbType.VarChar).Value= p_durl   ;
                        cmd.Parameters.Add("@p_tser", SqlDbType.SmallInt).Value = p_tser ;
                        cmd.Parameters.Add("@p_tipo", SqlDbType.SmallInt).Value= p_tipo  ;
                        cmd.Parameters.Add("@p_ferr", SqlDbType.SmallInt).Value= p_ferr  ;
                        cmd.Parameters.Add("@p_oerr", SqlDbType.SmallInt).Value= p_oerr  ;
                        cmd.Parameters.Add("@p_erro", SqlDbType.VarChar).Value= p_erro  ;
                        cmd.Parameters.Add("@p_derr", SqlDbType.VarChar).Value= p_derr  ;
                        cmd.Parameters.Add("@p_fech", SqlDbType.Date).Value = p_fech;
                        cmd.Parameters.Add("@p_hini", SqlDbType.VarChar).Value= p_hini  ;
                        cmd.Parameters.Add("@p_date", SqlDbType.VarChar).Value=  p_date ;
                        cmd.Parameters.Add("@p_hfin", SqlDbType.VarChar).Value= p_hfin  ;
                        cmd.Parameters.Add("@p_dats", SqlDbType.VarChar).Value= p_dats  ;
                        cmd.Parameters.Add("@p_user", SqlDbType.VarChar).Value= p_user  ;

                        cmd.CommandTimeout = EN_Constante.g_const_0;
                        dr = cmd.ExecuteReader();

                        while(dr.Read())
                        {
                            if(dr["estado"] != DBNull.Value && Convert.ToInt32(dr["estado"].ToString())== EN_Constante.g_const_1)
                            {
                                return Convert.ToInt32(dr["codigo"].ToString());
                            }
                            else
                            {
                                return EN_Constante.g_const_0;
                            }


                        }

                    }
                    dr.Close();
                    }
                
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();

            }
            return ntraLog;
            
        }

        public bool registrar_log_fin(int p_codi, int p_ferr, int p_oerr, string p_erro, string p_derr, string p_hfin,string p_dats, string p_date)
        {

            //DESCRIPCION: funcion para registrar log de fin
            AD_Cado CadCx = new AD_Cado(); // CONEXION
            SqlDataReader dr; //Data reader
            try
            {
                using (_sqlConexion = new SqlConnection(CadCx.CadenaConexion()))
                {
                    _sqlConexion.Open();
                    using (SqlCommand Comando = new SqlCommand("Usp_registrar_log_fin_webapi", _sqlConexion))
                    {
                        Comando.CommandType = CommandType.StoredProcedure;

                        Comando.Parameters.Add("@p_codi", SqlDbType.Int).Value = p_codi;
                        Comando.Parameters.Add("@p_ferr", SqlDbType.SmallInt).Value = p_ferr;
                        Comando.Parameters.Add("@p_oerr", SqlDbType.SmallInt).Value = p_oerr;
                        Comando.Parameters.Add("@p_erro", SqlDbType.Char).Value = p_erro;
                        Comando.Parameters.Add("@p_derr", SqlDbType.Char).Value = p_derr;
                        Comando.Parameters.Add("@p_hfin", SqlDbType.Char).Value = p_hfin;
                        Comando.Parameters.Add("@p_dats", SqlDbType.Char).Value = p_dats;
                        Comando.Parameters.Add("@p_date", SqlDbType.Char).Value = p_date;
                        

                        Comando.CommandTimeout = EN_Constante.g_const_0;
                        dr = Comando.ExecuteReader();

                        while (dr.Read())
                        {
                            if (dr["estado"] != DBNull.Value && Convert.ToInt32(dr["estado"].ToString()) == EN_Constante.g_const_1)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }

                        }

                    }
                    dr.Close();
                    
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                _sqlConexion.Close();
            }

        }
    }
}
