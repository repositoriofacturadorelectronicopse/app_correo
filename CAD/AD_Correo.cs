/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Correo.cs
 VERSION : 1.0
 OBJETIVO: Clase de conexión correo
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CEN;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
namespace CAD
{
    public class AD_Correo
    {
         private string pClase = "AD_Correo";
        private readonly AD_Cado _datosConexion;        // clase de acceso cado
        private readonly SqlConnection _sqlConexion;    // conexión sql
        private readonly SqlCommand _sqlCommand;        // comando
        private string TiempoEspera {get;}


        public AD_Correo()
        {
             //DESCRIPCION: CONSTRUCTOR DE CLASE
            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
            TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
        
        }

        public List<EN_Documento> listarDocsCorreo()
        {
            //DESCRIPCION: FUNCION LISTA DOCUMENTOS PENDIENTES DE ENVIO POR CORREO ELECTRÓNICO

            List<EN_Documento> olstDP = new List<EN_Documento>(); //lista de clase entidad documento
            EN_Documento oDP_EN;                //clase entidad documento
            SqlCommand cmd ;                    // comando
            SqlDataReader Rs;                   // data reader

            try
            {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_docsCorreo",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;

                    Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDP_EN = new EN_Documento();
                        {
                            var withBlock = oDP_EN;
                            withBlock.Id =(string) Rs["id"];
                            withBlock.Idempresa =(int) Rs["empresa_id"];
                            withBlock.Idtipodocumento =(string) Rs["tipodocumento_id"];
                            withBlock.Tipodocumento= (string)Rs["tipodocumento"];
                            withBlock.Serie = (string) Rs["serie_comprobante"];
                            withBlock.Numero =(string) Rs["nro_comprobante"];
                            withBlock.Idpuntoventa =(int)Interaction.IIf(Information.IsDBNull(Rs["puntoventa_id"]), (object)0, Rs["puntoventa_id"]);
                            withBlock.Emailcliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_email"]), "", Rs["cliente_email"]);
                            withBlock.Fechavencimiento =(string)Interaction.IIf(Information.IsDBNull(Rs["fechavencimientopago"].ToString()), "", Rs["fechavencimientopago"].ToString());
                            withBlock.EnviadoMail =(string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                            withBlock.Fecha =Convert.ToString(Rs["fecha"]);
                            withBlock.Hora =Convert.ToString(Rs["hora"]);
                             withBlock.Estado =(string) Rs["estado"];
                            withBlock.Situacion =(string) Rs["situacion"];
                            withBlock.Importefinal= (double)Rs["importefinal"];
                            withBlock.Nombrecliente= (string)Rs["cliente_nombre"];
                        }
                        olstDP.Add(oDP_EN);
                    }
                }
                Rs.Close();
                return olstDP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

          public List<EN_OtrosCpe> listarDocsOtroscpeCorreo()
        {
            //DESCRIPCION: FUNCION LISTA DOCUMENTOS PENDIENTES DE ENVIO POR CORREO ELECTRÓNICO

            List<EN_OtrosCpe> olstDP = new List<EN_OtrosCpe>(); //lista de clase entidad documento
            EN_OtrosCpe oDP_EN;                //clase entidad documento
            SqlCommand cmd ;                    // comando
            SqlDataReader Rs;                   // data reader

            try
            {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_otrosCpeCorreo",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;

                    Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDP_EN = new EN_OtrosCpe();
                        {
                      

                            var withBlock = oDP_EN;
                            withBlock.id =(string) Rs["id"];
                            withBlock.empresa_id =(int) Rs["empresa_id"];
                            withBlock.tipodocumento_id =(string) Rs["tipodocumento_id"];
                            withBlock.desctipodocumento= (string)Rs["tipodocumento"];
                            withBlock.serie = (string) Rs["serie"];
                            withBlock.numero =(string) Rs["numero"];
                            withBlock.puntoventa_id =(int)Interaction.IIf(Information.IsDBNull(Rs["puntoventa_id"]), (object)0, Rs["puntoventa_id"]);
                            withBlock.persona_email =(string)Interaction.IIf(Information.IsDBNull(Rs["persona_email"]), "", Rs["persona_email"]);
                            
                            withBlock.enviado_email =(string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                            withBlock.fechaemision =Convert.ToString(Rs["fechaemision"]);
                         
                             withBlock.estado =(string) Rs["estado"];
                            withBlock.situacion =(string) Rs["situacion"];
                            withBlock.importe_retper= (decimal)Rs["importe_retper"];
                            withBlock.persona_nombrecomercial= (string)Rs["persona_nombrecomercial"];
                        }
                        olstDP.Add(oDP_EN);
                    }
                }
                Rs.Close();
                return olstDP;
            }
            catch (Exception ex)
            {
                throw new Exception(pClase+ ", listarDocsOtroscpeCorreo. "+ex);
                
            }
            finally
            {
                _sqlConexion.Close();
            }
        }


         public List<EN_Tbllog> listarDocsCorreoError()
        {
            //DESCRIPCION: FUNCION LISTA DOCUMENTOS PENDIENTES DE ENVIO POR CORREO ELECTRÓNICO

            List<EN_Tbllog> olstDP = new List<EN_Tbllog>(); //lista de clase entidad documento
            EN_Tbllog oDP_EN;                //clase entidad documento
            SqlCommand cmd ;                    // comando
            SqlDataReader Rs;                   // data reader

            try
            {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_erroresCorreo",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;

                    Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDP_EN = new EN_Tbllog();
                        {
                            var withBlock = oDP_EN;

                            withBlock.logcodi  = (int)Interaction.IIf(Information.IsDBNull(Rs["logcodi"]), (object)0, Rs["logcodi"]);
                            withBlock.logprog =(string)Interaction.IIf(Information.IsDBNull(Rs["logprog"]), "", Rs["logprog"]);
                            withBlock.logidoc =(string)Interaction.IIf(Information.IsDBNull(Rs["logidoc"]), "", Rs["logidoc"]);
                            withBlock.logtdoc =(string)Interaction.IIf(Information.IsDBNull(Rs["logtdoc"]), "", Rs["logtdoc"]);
                            withBlock.logiemp  =(int)Interaction.IIf(Information.IsDBNull(Rs["logiemp"]), (object)0, Convert.ToInt32(Rs["logiemp"]));
                            withBlock.logipvt  =(int)Interaction.IIf(Information.IsDBNull(Rs["logipvt"]), (object)0, Convert.ToInt32(Rs["logipvt"]));
                            withBlock.lognemp  =(string)Interaction.IIf(Information.IsDBNull(Rs["lognemp"]), "", Rs["lognemp"]);
                            withBlock.lognive  =(int)Interaction.IIf(Information.IsDBNull(Rs["lognive"]), (object)0, Convert.ToInt32(Rs["lognive"]));
                            withBlock.lognwba  =(string)Interaction.IIf(Information.IsDBNull(Rs["lognwba"]), "", Rs["lognwba"]);
                            withBlock.logdurl  =(string)Interaction.IIf(Information.IsDBNull(Rs["logdurl"]), "", Rs["logdurl"]);
                            withBlock.logtser  =(int)Interaction.IIf(Information.IsDBNull(Rs["logtser"]), (object)0, Convert.ToInt32(Rs["logtser"]));
                            withBlock.logtipo  =(int)Interaction.IIf(Information.IsDBNull(Rs["logtipo"]), (object)0, Convert.ToInt32(Rs["logtipo"]));
                            withBlock.logferr  =(int)Interaction.IIf(Information.IsDBNull(Rs["logferr"]), (object)0, Convert.ToInt32(Rs["logferr"]));
                            withBlock.logoerr  =(int)Interaction.IIf(Information.IsDBNull(Rs["logoerr"]), (object)0, Convert.ToInt32(Rs["logoerr"]));
                            withBlock.logerro  =(string)Interaction.IIf(Information.IsDBNull(Rs["logerro"]), "", Rs["logerro"]);
                            withBlock.logderr  =(string)Interaction.IIf(Information.IsDBNull(Rs["logderr"]), "", Rs["logderr"]);
                            withBlock.logfech   =(string)Interaction.IIf(Information.IsDBNull(Rs["logfech"]), "",Strings.Format(Conversions.ToDate(Rs["logfech"]), "yyyy-MM-dd"));
                            withBlock.loghini  =(string)Interaction.IIf(Information.IsDBNull(Rs["loghini"]), "", Rs["loghini"]);
                            withBlock.logdate  =(string)Interaction.IIf(Information.IsDBNull(Rs["logdate"]), "", Rs["logdate"]);
                            withBlock.loghfin  =(string)Interaction.IIf(Information.IsDBNull(Rs["loghfin"]), "", Rs["loghfin"]);
                            withBlock.logdats   =(string)Interaction.IIf(Information.IsDBNull(Rs["logdats"]), "", Rs["logdats"]);
                            withBlock.loguser  =(string)Interaction.IIf(Information.IsDBNull(Rs["loguser"]), "", Rs["loguser"]);
                        }
                        olstDP.Add(oDP_EN);
                    }
                }
                Rs.Close();
                return olstDP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }



    public List<EN_Correo> listarCorreo(EN_Correo pCorreo)
    {
        //DESCRIPCION: FUNCION LISTA CORREO DE EMPRESA
        List<EN_Correo> olstCorreo = new List<EN_Correo>();     // lista de clase entidad correo
        EN_Correo oCorreo_EN;               // clase entidad correo

        SqlCommand cmd ;                    // comando
        SqlDataReader Rs;                   // data reader
        try
        {
            _sqlConexion.Open();
            cmd = new SqlCommand("Usp_listar_correoEmpresa",_sqlConexion);
            cmd.CommandType= CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_id",  pCorreo.id);
            cmd.Parameters.AddWithValue("@p_empresaid", pCorreo.empresa_id);
            cmd.Parameters.AddWithValue("@p_cuentaemail",  pCorreo.cuenta_email);
            cmd.Parameters.AddWithValue("@p_default",  pCorreo.es_default);

            Rs = cmd.ExecuteReader();

            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oCorreo_EN = new EN_Correo();
                    {
                        var withBlock = oCorreo_EN;
                        withBlock.id =(int)Rs["id"];
                        withBlock.empresa_id = (int)Rs["empresa_id"];
                        withBlock.cuenta_email = (string)Interaction.IIf(Information.IsDBNull(Rs["cuenta_email"]), "", Rs["cuenta_email"]);
                        withBlock.nombre_email = (string)Interaction.IIf(Information.IsDBNull(Rs["nombre_email"]), "", Rs["nombre_email"]);
                        withBlock.clave_email = (string)Interaction.IIf(Information.IsDBNull(Rs["clave_email"]), "", Rs["clave_email"]);
                        withBlock.cuenta_smtp = (string)Interaction.IIf(Information.IsDBNull(Rs["cuenta_smtp"]), "", Rs["cuenta_smtp"]);
                        withBlock.puerto_smtp = (int)Interaction.IIf(Information.IsDBNull(Rs["puerto_smtp"]), 587, Rs["puerto_smtp"]);
                        withBlock.link_descarga = (string)Interaction.IIf(Information.IsDBNull(Rs["link_descarga"]), "", Rs["link_descarga"]);
                    }
                    olstCorreo.Add(oCorreo_EN);
                }
            }
            Rs.Close();
            return olstCorreo;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
        
    }
}