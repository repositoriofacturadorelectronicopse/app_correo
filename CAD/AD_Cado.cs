﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Cado.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a cadena de conexion a base de datos SQL
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using Microsoft.EntityFrameworkCore;
using CEN;
namespace CAD
{
    public class AD_Cado: DbContext
    {
        
        public AD_Cado() {
             //DESCRIPCION: CONSTRUCTOR DE CLASE
         }
     
        public string CadenaConexion()
        {
            try
            {
                string cadena;      // cadena de conexión
                cadena=EN_ConfigConstantes.Instance.const_cadenaCnxBdFE;
                return cadena;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    
    }
}
