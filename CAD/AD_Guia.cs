using CEN;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
namespace CAD
{
    public class AD_Guia
    {
        private SqlConnection connection;   // Variable de conexion

        public EN_RespuestaListaCorreoGuia listarCorreoGuia() 
        {
            EN_RespuestaListaCorreoGuia respuesta = new EN_RespuestaListaCorreoGuia();
            AD_Conector conector = new AD_Conector();
            SqlDataReader dr = null;
            try
            {
                using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Command = new SqlCommand("Usp_listar_correos_guia", connection)) 
                    {
                            Command.CommandType = CommandType.StoredProcedure;
                            Command.CommandTimeout = EN_Constante.g_const_0;
                              dr = Command.ExecuteReader();
                            while(dr.Read()) 
                            {
                                EN_Guia guia = new EN_Guia();
                                 if (dr["id"] != null)
                                guia.id = dr["id"].ToString();

                                if(dr["empresa_id"] != null)
                                guia.empresa_id = Convert.ToInt32(dr["empresa_id"].ToString());

                                if (dr["tipodocumento_id"] != null)
                                guia.tipodocumento_id = dr["tipodocumento_id"].ToString();

                                if (dr["serie"] != null)
                                guia.serie = dr["serie"].ToString();

                                if (dr["numero"] != null)
                                guia.numero = dr["numero"].ToString();

                                if (dr["puntoemision"] != null && dr["puntoemision"].ToString() != "")
                                guia.puntoemision = Convert.ToInt32(dr["puntoemision"].ToString());

                                if (dr["destinatario_email"] != null)
                                guia.destinatario_email = dr["destinatario_email"].ToString();

                                if (dr["fechaemision"] != null)
                                guia.fechaemision = dr["fechaemision"].ToString();

                                if (dr["horaemision"] != null)
                                guia.horaemision = dr["horaemision"].ToString();

                                if (dr["estado"] != null)
                                guia.estado = dr["estado"].ToString();

                                if (dr["situacion"] != null)
                                guia.situacion = dr["situacion"].ToString();

                                if (dr["enviado_email"] != null)
                                guia.enviado_email = dr["enviado_email"].ToString();

                                if (dr["destinatario_nombre"] != null)
                                guia.destinatario_nombre = dr["destinatario_nombre"].ToString();

                                if (dr["tipodocumento"] != null)
                                guia.tipodocumento = dr["tipodocumento"].ToString();

                                respuesta.listDetGuia.Add(guia);


                            }
                            if(respuesta.listDetGuia.Count > EN_Constante.g_const_0) {
                                respuesta.ResplistaGuia.FlagVerificacion = true;
                                respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                                respuesta.ResplistaGuia.dato = EN_Constante.g_const_2000;
                            } else {
                                respuesta.ResplistaGuia.FlagVerificacion = false;
                                respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_correo_notFound;
                                respuesta.ResplistaGuia.dato = EN_Constante.g_const_1000;

                            }
                    }
                    dr.Close();
                }
                 return respuesta;
            }
            catch (Exception ex)
            {
                
                respuesta.ResplistaGuia.FlagVerificacion = false;
                respuesta.ResplistaGuia.DescRespuesta = ex.Message;
                respuesta.ResplistaGuia.dato = EN_Constante.g_const_3000;
                return respuesta;
            }
            finally 
            {
                connection.Close();
            }

        }
        public EN_RespuestaListaCorreo listarCorreoGuia(EN_Correo pCorreo) 
        {
            EN_RespuestaListaCorreo respuestaCorreo = new EN_RespuestaListaCorreo();
            AD_Conector conector = new AD_Conector();
            SqlDataReader dr = null;
            try
            {
                using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Command = new SqlCommand("Usp_listar_correoEmpresa", connection)) 
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_id", SqlDbType.Int).Value = pCorreo.id;
                        Command.Parameters.Add("@p_empresaid", SqlDbType.Int).Value = pCorreo.empresa_id;
                        Command.Parameters.Add("@p_cuentaemail", SqlDbType.VarChar,250).Value = pCorreo.cuenta_email;
                        Command.CommandTimeout = EN_Constante.g_const_0;
                         dr = Command.ExecuteReader();
                         while(dr.Read()) 
                         {
                                EN_Correo correo = new EN_Correo();
                                 if (dr["id"] != null && dr["id"].ToString() != "")
                                correo.id = Convert.ToInt32(dr["id"].ToString());

                                if(dr["empresa_id"] != null)
                                correo.empresa_id = Convert.ToInt32(dr["empresa_id"].ToString());

                                if (dr["cuenta_email"] != null)
                                correo.cuenta_email = dr["cuenta_email"].ToString();

                                if (dr["nombre_email"] != null)
                                correo.nombre_email = dr["nombre_email"].ToString();

                                if (dr["clave_email"] != null)
                                correo.clave_email = dr["clave_email"].ToString();

                                if (dr["cuenta_smtp"] != null)
                                correo.cuenta_smtp = dr["cuenta_smtp"].ToString();

                                if (dr["puerto_smtp"] != null && dr["puerto_smtp"].ToString() != "")
                                correo.puerto_smtp = Convert.ToInt32(dr["puerto_smtp"].ToString());

                                if (dr["link_descarga"] != null)
                                correo.link_descarga = dr["link_descarga"].ToString();

                                respuestaCorreo.listDetGuia.Add(correo);

                         }
                             if(respuestaCorreo.listDetGuia.Count > EN_Constante.g_const_0) {
                                respuestaCorreo.ResplistaGuia.FlagVerificacion = true;
                                respuestaCorreo.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                                respuestaCorreo.ResplistaGuia.dato = EN_Constante.g_const_2000;
                            } else {
                                respuestaCorreo.ResplistaGuia.FlagVerificacion = false;
                                respuestaCorreo.ResplistaGuia.DescRespuesta = EN_Constante.g_const_correo_empresa_notFound;
                                respuestaCorreo.ResplistaGuia.dato = EN_Constante.g_const_1000;

                            }

                    }
                    dr.Close();
                }
                 return respuestaCorreo;
            }
            catch (Exception ex)
            {
                
                respuestaCorreo.ResplistaGuia.FlagVerificacion = false;
                respuestaCorreo.ResplistaGuia.DescRespuesta = ex.Message;
                respuestaCorreo.ResplistaGuia.dato = EN_Constante.g_const_3000;
                return respuestaCorreo;
            }
              finally 
            {
                connection.Close();
            }


        }
        public EN_RespuestaData actualizarEnvioEmail(EN_Guia objGuia,string estado) 
        {
            EN_RespuestaData respuesta = new EN_RespuestaData();
            AD_Conector conector = new AD_Conector();
            SqlDataReader dr = null;
            try
            {
                 using (connection = new SqlConnection(conector.CxSQLFacturacion()))
                {
                    connection.Open();
                    using (SqlCommand Command = new SqlCommand("Usp_actualizar_guia_envioEmail", connection)) 
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@empresa_id", SqlDbType.Int).Value = objGuia.empresa_id;
                        Command.Parameters.Add("@id", SqlDbType.VarChar,15).Value = objGuia.id;
                        Command.Parameters.Add("@tipodocumento_id", SqlDbType.VarChar,2).Value = objGuia.tipodocumento_id;
                        Command.Parameters.Add("@estado", SqlDbType.Char,2).Value = estado;
                        Command.CommandTimeout = EN_Constante.g_const_0;
                         dr = Command.ExecuteReader();
                         while(dr.Read()) 
                         {
                            if (dr["codigo"] != null && dr["codigo"].ToString() != "" && 
                               Convert.ToInt32(dr["codigo"].ToString()) == EN_Constante.g_const_1)
                            {
                                respuesta.ResplistaGuia.FlagVerificacion = true;
                                respuesta.ResplistaGuia.DescRespuesta = dr["mensaje"].ToString();
                                respuesta.errorService.TipoError = EN_Constante.g_const_0;
                                respuesta.errorService.CodigoError = EN_Constante.g_const_2000;
                                respuesta.errorService.DescripcionErr = EN_Constante.g_update_exito;
                            } else {
                                respuesta.ResplistaGuia.FlagVerificacion = false;
                                respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                                respuesta.errorService.TipoError = EN_Constante.g_const_1;
                                respuesta.errorService.CodigoError = EN_Constante.g_const_3000;
                                respuesta.errorService.DescripcionErr = dr["mensaje"].ToString();
                            }
                               

                         }

                    }
                    dr.Close();
                }
              return respuesta;   
            }
            catch (Exception ex)
            {
                
                respuesta.ResplistaGuia.FlagVerificacion = false;
                respuesta.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                respuesta.errorService.TipoError = EN_Constante.g_const_1;
                respuesta.errorService.CodigoError = EN_Constante.g_const_3000;
                respuesta.errorService.DescripcionErr = ex.Message;
                return respuesta; 
            }
             finally 
            {
                connection.Close();
            }


        }
    }
}