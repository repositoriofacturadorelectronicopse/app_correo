﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Facturacion.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos facturación
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic.CompilerServices; // Install-Package Microsoft.VisualBasic
using CEN;
using System.Text;

namespace CAD
{

 
        public partial class AD_Facturacion
        {
            private readonly AD_Cado _datosConexion;
            private readonly SqlConnection _sqlConexion;
            private readonly SqlCommand _sqlCommand;
             private string TiempoEspera {get;}

            public AD_Facturacion()
            {
                _datosConexion = new AD_Cado();
                _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
                _sqlCommand = new SqlCommand();
                 TiempoEspera=EN_ConfigConstantes.Instance.const_TiempoEspera;
            }

            public string buscarParametroAppSettings(int codconcepto, int codcorrelativo)
            {
                // DESCRIPCION: BUSCAR CONSTANTES EN TABLA SUNAT
                
                SqlCommand cmd ; //comando
                SqlDataReader reader ; //Data reader
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@flag", 0);
                    cmd.Parameters.AddWithValue("@idBusqueda1", codconcepto);
                    cmd.Parameters.AddWithValue("@idBusqueda2", codcorrelativo);

                    reader = cmd.ExecuteReader();

                    string response="";
                    while(reader.Read())
                    {
                        response = (reader["par_descripcion"]==null)?"":reader["par_descripcion"].ToString();
                    }

                    return response;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }
                
            }
            public string buscarConstantesTablaSunat(string codtabla,string codalterno)
            {
                // DESCRIPCION: BUSCAR CONSTANTES EN TABLA SUNAT

                SqlCommand cmd ; //comando
                SqlDataReader reader ; //Data reader
                 StringBuilder bld ;         // ACUMULADOR DE STRING  
                 int contador=0;             // CONTADOR
                 string response="";         // RESPUESTA
                try 
                {
                    
            
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento

                    cmd.Parameters.AddWithValue("@flag", 0);
                    cmd.Parameters.AddWithValue("@codtabla", codtabla);
                    cmd.Parameters.AddWithValue("@codbusqueda", codalterno);

                    reader = cmd.ExecuteReader();
                    bld = new StringBuilder();

                    while(reader.Read())
                    {
                        if(codalterno=="RG"){
                            if(contador!=0)
                            {
                                bld.Append(",");
                            }
                               bld.Append("'"+reader["descpcampo"].ToString()+"'");
                               contador++;
                        }else{
                              bld.Append(reader["descpcampo"].ToString());
                        }
                    }
                    
                    response = bld.ToString();

                    return response;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }

            }

            public string ObtenerCodigoSunat(string codtabla, string codigo)
            {
                try
                {
                    string CodigoSunat = "";
               

                      _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 1);
                    cmd.Parameters.AddWithValue("@codtabla", codtabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda", codigo.Trim());

                    SqlDataReader Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            CodigoSunat = Conversions.ToString(Rs["codsunat"]);
                    }

                    Rs.Close();
                    return CodigoSunat.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public string ObtenerDescripcionPorCodSunat(string strCodTabla, string strCodSunat)
            {
                try
                {
                    string descripcion = "";
                  _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 2);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSunat.Trim());

                    SqlDataReader Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            descripcion = Conversions.ToString(Rs["descripcion"]);
                    }

                    Rs.Close();
                    return descripcion.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public string ObtenerDescripcionPorCodElemento(string strCodTabla, string strCodSistema)
            {
                try
                {
                    string descripcion = "";
              
                 _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 3);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSistema.Trim());

                    SqlDataReader Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            descripcion = Conversions.ToString(Rs["descripcion"]);
                    }

                    Rs.Close();
                    return descripcion.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public string ObtenerCodigoAlternoPorCodigoSistema(string strCodTabla, string strCodSistema)
            {
                try
                {
                    string CodigoSunat = "";
             
                 _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 4);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSistema.Trim());

                    SqlDataReader Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            CodigoSunat = Conversions.ToString(Rs["codalterno"]);
                    }

                    Rs.Close();
                    return CodigoSunat.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

          


           

           


            


    


        public void ActualizarEnvioMail(EN_Documento oDocumento, string estado)
        {
        
            try
            {
                string nrocomprobElect = oDocumento.Serie + "-" + oDocumento.Numero;
                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_actualizar_facturaenviomail";
                _sqlCommand.Parameters.AddWithValue("@empresa_id", oDocumento.Idempresa);
                _sqlCommand.Parameters.AddWithValue("@documento_id", oDocumento.Id);
                _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oDocumento.Idtipodocumento);
                _sqlCommand.Parameters.AddWithValue("@estado", estado);
                _sqlCommand.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public void ActualizarEnvioMailOtroscpe(EN_OtrosCpe oDocumento, string estado)
        {
        
            try
            {
                string nrocomprobElect = oDocumento.serie + "-" + oDocumento.numero;
                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_actualizar_otroscpeenviomail";
                _sqlCommand.Parameters.AddWithValue("@empresa_id", oDocumento.empresa_id);
                _sqlCommand.Parameters.AddWithValue("@documento_id", oDocumento.id);
                _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oDocumento.tipodocumento_id);
                _sqlCommand.Parameters.AddWithValue("@estado", estado);
                _sqlCommand.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public void ActualizarEnvioError(EN_Tbllog oDocumento, string estado)
        {
        
            try
            {
                
                _sqlConexion.Open();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_actualizar_tbllogMail";
                _sqlCommand.Parameters.AddWithValue("@id", oDocumento.logcodi);
                _sqlCommand.Parameters.AddWithValue("@estado", estado);
                _sqlCommand.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }


        
        }
    
}
